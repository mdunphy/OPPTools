import os
import warnings
import psutil
import fnmatch
import numpy as np
import scipy
from scipy import io as sio
import datetime
from matplotlib.dates import date2num
import netCDF4 as nc
from pyproj import Proj
import shapely.geometry as geom
import gsw # https://teos-10.github.io/GSW-Python/install.html
import OPPTools.fvcomToolbox as fvt
from OPPTools.general_utilities import mut
from OPPTools.general_utilities import geometry as mgeom
from OPPTools.general_utilities import interpolation as interp
from OPPTools.mesh import mesh as mesh


def read_metrics(fgrd,fbathy,fsigma,fnemocoord,fnemomask,fnemobathy,
                 nemo_cut_i=None,nemo_cut_j=None):
    """ Read static grid info for fvcom and nemo grids.
    Inputs:
        fgrd,fbathy,fsigma                  standard FVCOM3 input files
        fnemocoord,fnemomask,fnemobathy     files containing NEMO grid metrics
                                            for the entire NEMO grid
        nemo_cut_i,nemo_cut_j       (2,)    start and end indices (in Python sense)
                                            along x- and y-axes
                                            if NEMO solution is saved on a
                                            subset of the entire NEMO grid;
                                            optional; by default, the solution 
                                            is on the entire grid
    Outputs:
        variables required for make_type3_nesting_file2 and make_its
    """
    
    # fvcom grid parameters
    tri, node_xy = fvt.readMesh_V3(fgrd) # tri is 1-based
    x = node_xy[:,0]
    y = node_xy[:,1]
    z = np.loadtxt(fbathy, skiprows=1, usecols=(2,), dtype='float')
    nsiglev, siglev, nsiglay, siglay = fvt.read_sigma(fsigma)
    
    # ensure 1D arrays: 
    # fvt.read_sigma can yield both 1D and 2D siglay/siglev arrays depending on 
    # sigma-layer distribution type, e.g. 1D for geometric and 2D for tanh
    siglay = np.squeeze(siglay)
    siglev = np.squeeze(siglev)
    
    # NEMO grid parameters
    coordfile = fnemocoord
    if coordfile is None:
        coordfile = fnemomask
    with nc.Dataset(coordfile) as nccoord:
        nemo_lon = nccoord.variables['nav_lon'][:]
        nemo_lat = nccoord.variables['nav_lat'][:]
        # grid spacing in x- and y-direction;
        # for u- and v-grids, grid spacing is same as for t-grid, usually within 1 cm
        # (except fraser river inset, which is not overlapping with the nesting zone)
        e1t = nccoord.variables['e1t'][0,:,:]
        e2t = nccoord.variables['e2t'][0,:,:]
    
    with nc.Dataset(fnemomask) as ncmask:
        # only one value along time dimension
        e3u_0 = ncmask.variables['e3u_0'][0,:,:,:] # grid spacing on U-grid in w direction
        e3v_0 = ncmask.variables['e3v_0'][0,:,:,:] # grid spacing on V-grid in w direction
        
        # for baroclinic: NEMO zsl with adjusted near-bottom layer value for each node
        gdept_0 = ncmask.variables['gdept_0'][0,:,:,:] # depth of T-grid points (t,z,y,x)
        gdepw_0 = ncmask.variables['gdepw_0'][0,:,:,:] # depth of W-grid points (t,z,y,x)
        gdepu   = ncmask.variables['gdepu'][0,:,:,:] # depth of U-grid points (t,z,y,x)
        gdepv   = ncmask.variables['gdepv'][0,:,:,:] # depth of V-grid points (t,z,y,x)
        tmask   = ncmask.variables['tmask'][0,:,:,:] # mask for T-grid and W-grid (t,z,y,x)
        umask   = ncmask.variables['umask'][0,:,:,:] # mask for U-grid (t,z,y,x)
        vmask   = ncmask.variables['vmask'][0,:,:,:] # mask for V-grid (t,z,y,x)
        gdept_1d= ncmask.variables['gdept_1d'][0,:]  # nominal depth of T-grid points (t,z)
    
    with nc.Dataset(fnemobathy) as ncbathy:
        nemo_h = ncbathy.variables['Bathymetry'][:] # sea_floor_depth
    
    # cut nemo metrics to the output area if necessary
    if nemo_cut_i is not None:
        nemo_lon = nemo_lon[  nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        nemo_lat = nemo_lat[  nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        e1t      =      e1t[  nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        e2t      =      e2t[  nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        e3u_0    =    e3u_0[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        e3v_0    =    e3v_0[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        gdept_0  =  gdept_0[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        gdepw_0  =  gdepw_0[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        gdepu    =    gdepu[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        gdepv    =    gdepv[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        tmask    =    tmask[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        umask    =    umask[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        vmask    =    vmask[:,nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
        nemo_h   =   nemo_h[  nemo_cut_j[0]:nemo_cut_j[1], nemo_cut_i[0]:nemo_cut_i[1]]
    
    return x,y,z,tri,nsiglev,siglev,nsiglay,siglay,\
           nemo_lon,nemo_lat,e1t,e2t,e3u_0,e3v_0,\
           gdept_0,gdepw_0,gdepu,gdepv,tmask,umask,vmask,gdept_1d,nemo_h


def read_nesting(fnest,frefline):
    """ Read nesting zone info, variables required for make_type3_nesting_file2.
    Inputs:
        fnest       str     ascii file with indices of fvcom nodes in the nesting zone
        frefline    str     ascii file with reference polyline for nesting zone weight calculation
    Outputs:
        inest       (n,)    indices of fvcom nodes in the nesting zone
        xb,yb       (m,)    reference polyline for nesting zone weight calculation
    """
    
    # indices of fvcom nodes in the nesting zone
    inest = np.genfromtxt(fnest,dtype=int)
    inest -= 1 # make zero-based
    
    # reference polyline for nesting zone weight calculation
    xb,yb = np.genfromtxt(frefline,delimiter=',',usecols=(0,1),unpack=True)
    
    return inest,xb,yb


def make_type3_nesting_file2(fout, x,y,z,tri,nsiglev,siglev,nsiglay,siglay, utmzone,
                            inest, xb,yb,rwidth,dl,du,
                            nemo_lon,nemo_lat, e1t,e2t, 
                            e3u_0,e3v_0,
                            nemo_file_list,
                            time_start = None, time_end = None,
                            opt='BRTP', nrampup=0, TBRTP=10.0, SBRTP=35.0,
                            gdept_0=None, gdepw_0=None, gdepu=None, gdepv=None,
                            tmask=None, umask=None, vmask=None,
                            ua_name=None, va_name=None,
                            u_name=None,v_name=None,w_name=None,
                            t_name=None,s_name=None):
    """ Generation of FVCOM Type 3 nesting file for FVCOM from a structured grid z-level model (NEMO) output.
    FVCOM nodes in the nesting zone should be matched with NEMO nodes.
    Time step for the forcing series corresponds to the NEMO output time step.
    Calculates the necessary static variables and calls the helper.
    Inputs:
        fout    str         nesting file name
        x,y     (nnode,)    FVCOM node coordinates
        z       (nnode,)    FVCOM bathymetry
        tri     (nelem,3)   triangulation array for FVCOM grid (1-based indices)
        nsiglev int         number of FVCOM sigma-levels
        siglev  (nsiglev,)  FVCOM sigma-levels (no support for hybrid coordinate option)
        nsiglay int         number of FVCOM sigma-layers
        siglay  (nsiglay,)  FVCOM sigma-layers
        inest               indices of fvcom nodes in the nesting zone
        xb,yb               reference polyline for nesting zone weight calculation
                            (can be nan-separated)
        rwidth              width (m) of the zone where nesting weights transition from 0 to 1
        dl,du               parameters for the tanh function defining the transition profile
        nemo_lon,nemo_lat   NEMO T-grid coordinates
        e1t,e2t             NEMO grid spacing in x- and y-direction
        e3u_0,e3v_0         vertical grid spacing on NEMO U- and V-grids
        nemo_file_list      list of "T" NEMO output files; 
                            U,V,W file names must differ only in the 4th character from the end
        time_start,time_end     str, FVCOM simulation start and end times in 
                                '%Y-%m-%d %H:%M:%S' format
        opt                 'BRTP' (default) or 'BRCL', barotropic or baroclinic
        nrampup             number of time steps for FVCOM ramp-up (in time steps of NEMO output)
                            default is 0
        TBRTP,SBRTP         fill-in values for T,S for barotropic case
        gdept_0             depth of NEMO T-grid points (t,z,y,x)
        gdepw_0             depth of NEMO W-grid points (t,z,y,x)
        gdepu,gdepv         depth of NEMO U- and V-grid points (t,z,y,x)
        tmask               mask for NEMO T-grid and W-grid (t,z,y,x)
        umask,vmask         masks for NEMO U- and V-grids (t,z,y,x)
        ua_name,va_name     variable names for vertically averged velocity 
                            components if they are present in NEMO output files
        u_name,v_name,w_name    variable names for velocity components if 
                                different from standard NEMO names
                                (defaults are set in generate_type3_nesting_file)
        t_name,s_name       variable names for temperature and salinity if 
                            different from standard
    """
    
    # FVCOM metrics
    node_x, node_y, dep_noden, cell_x, cell_y, dep_elemn = fvcom_nesting_zone_coord(x,y,z,tri,inest)
    node_w = weight_from_distance_to_polyline(node_x,node_y, xb,yb, rwidth,dl=dl,du=du)
    cell_w = weight_from_distance_to_polyline(cell_x,cell_y, xb,yb, rwidth,dl=dl,du=du)
    
    utmproj = Proj("+proj=utm +zone="+str(utmzone))
    node_lon, node_lat = utmproj(node_x, node_y, inverse=True)
    cell_lon, cell_lat = utmproj(cell_x, cell_y, inverse=True)
    
    # NEMO metrics
    theta = nemo_azimuth(nemo_lon,nemo_lat,utmzone)

    nemo_x,nemo_y = utmproj(nemo_lon,nemo_lat)
    
    locz = nemo_nesting_zone(nemo_x,nemo_y, node_x,node_y)
    
    nemo_grid_shape = nemo_x.shape # (898,398) for the full Salish Sea grid
    
    locu,wu,locv,wv = uv_interpolant(locz,nemo_grid_shape,e1t,e2t,inest,tri)
    
    vwu,vwv = nemo_vertical_weight(e3u_0,e3v_0,umask,vmask)

    if opt == 'BRCL':
        ##### NB: for 1d siglay only; doesn't work for hybrid sigma-coords:
        zsign = -siglay[:,None]*dep_noden # (nsiglay,nznodes), positive depths
        zsiglev = -siglev[:,None]*dep_noden # (nsiglev,nznodes), positive depths
        zsige = -siglay[:,None]*dep_elemn # (nsiglay,nzelems), positive depths
        
        # take depths at nodes in the nesing zone
        j,i = np.unravel_index(locz, nemo_grid_shape)
        gdeptn = gdept_0[:,j,i]
        gdepwn = gdepw_0[:,j,i]
        
        # to sub values below bottom with the deepest valid value for correct 
        # application of uv interpolant
        usub = below_bottom_sub(umask)
        vsub = below_bottom_sub(vmask)
        
        gdepu = np.take(gdepu,usub)
        gdepv = np.take(gdepv,vsub)
        # get gdepu,gdepv at elements in the nesting zone (apply uv interpolant)
        nzsl = gdepu.shape[0] # number of nemo z-levels
        gdepue = np.sum( np.take(gdepu.reshape((nzsl,-1)),locu,axis=1) * wu, axis=2)
        gdepve = np.sum( np.take(gdepv.reshape((nzsl,-1)),locv,axis=1) * wv, axis=2)
        
        # velocity masks get 0 where all 4 NEMO nodes used for interpolation to 
        # each FVCOM element are masked
        umaske = np.sum( np.take(umask.reshape((nzsl,-1)),locu,axis=1) * wu, axis=2)
        vmaske = np.sum( np.take(vmask.reshape((nzsl,-1)),locv,axis=1) * wv, axis=2)
        
        ivt1,ivt2,wvt1,wvt2 = interp.interpolant1d(zsign,gdeptn,tmask[:,j,i])
        ivw1,ivw2,wvw1,wvw2 = interp.interpolant1d(zsiglev,gdepwn,tmask[:,j,i])
        ivu1,ivu2,wvu1,wvu2 = interp.interpolant1d(zsige,gdepue,umaske)
        ivv1,ivv2,wvv1,wvv2 = interp.interpolant1d(zsige,gdepve,vmaske)
        
    else: # barotropic
        gdeptn=None; usub=None; vsub=None
        ivt1=None;ivt2=None;wvt1=None;wvt2=None
        ivu1=None;ivu2=None;wvu1=None;wvu2=None
        ivv1=None;ivv2=None;wvv1=None;wvv2=None
        ivw1=None;ivw2=None;wvw1=None;wvw2=None
        
    generate_type3_nesting_file(fout,
                            node_w, node_lon, node_lat, node_x, node_y, dep_noden,
                            cell_w, cell_lon, cell_lat, cell_x, cell_y, dep_elemn,
                            locz,locu,wu,locv,wv,
                            vwu,vwv, theta,
                            nsiglev, siglev, nsiglay, siglay,
                            nemo_file_list, time_start, time_end,
                            opt, nrampup, TBRTP, SBRTP,
                            gdeptn,usub,vsub,
                            ivt1,ivt2,wvt1,wvt2,
                            ivu1,ivu2,wvu1,wvu2,
                            ivv1,ivv2,wvv1,wvv2,
                            ivw1,ivw2,wvw1,wvw2,
                            ua_name,va_name,u_name,v_name,w_name,t_name,s_name)


def below_bottom_sub(mask):
    """ Substitution indices for the values below bottom.
    To sub values below bottom with the deepest valid value for correct 
    application of uv interpolant.
    Input:
        mask    (nemo_layers,y,x) e.g. umask, vmask
    Output:
        isub    (nemo_layers,y,x) indices for substitution
    """
    
    bmask = mask==0 # make boolean
    sz = mask.shape
    
    # layer indices, same for all nodes
    il = np.tile(np.arange(sz[0],dtype=int)[:,None,None],(sz[1],sz[2]))
    
    # mask indices below the bottom and find largest index for each node
    il[bmask] = 0
    ilm = np.argmax(il,axis=0) # same as mbathy for T-grid
    
    # array for substitution 
    ilz = np.tile(ilm[None,:,:],(sz[0],1,1))
    il[bmask] = ilz[bmask]
    
    # ravel indices for easier indexing
    i1 = np.tile(np.arange(sz[1])[None,:,None],(sz[0],1,sz[2]))
    i2 = np.tile(np.arange(sz[2])[None,None,:],(sz[0],sz[1],1))
    il = np.ravel_multi_index((il,i1,i2),sz)
    
    return il


def fvcom_nesting_zone_coord(x,y,z,tri,inest):
    """ FVCOM nesting zone coordinates from node indices for the nesting zone.
    
    Input:
        x,y,z,tri   FVCOM grid (tri is 1-based)
        inestn      nesting zone node indices (0-based)
        
    Output:
        xn,yn,zn    x,y,depth of nesting zone nodes
        xe,ye,ze    x,y,depth of nesting zone elements (centroids)
    """
    
    xn = x[inest]
    yn = y[inest]
    zn = z[inest]
    
    # triangles in the nesting zone: all nodes belong to nesting zone
    trin = tri[np.all(mut.ismember(tri, inest+1)[0],axis=1),:]
    xe = fvt.convertNodal2ElemVals(trin, x)
    ye = fvt.convertNodal2ElemVals(trin, y)
    ze = fvt.convertNodal2ElemVals(trin, z)
    
    return xn,yn,zn, xe,ye,ze
    

#def fvcom_nesting_weight(xn,yn,xb,yb,rwidth,dl=2,du=2):
def weight_from_distance_to_polyline(xn,yn,xb,yb,rwidth,linear=False,dl=2,du=2):
    """ Forcing weights for FVCOM nesting zone
    Tanh-function weights based on distance r from the inner boundary of the nesting zone.
    Weights range from 0 at the lower end of r to 1 at the higher end of r (at r>=rwidth).
    For proper forcing, rwidth should be less than min distance from inner to outer boundary.
    
    Input:
        xn,yn   nesting zone node coordinates
        xb,yb   polyline node coordinates (can be nan-separated)
        rwidth  radius of tanh-tansition zone
        du,dl   parameters controlling distribution of w as a function of r: 
                larger dl gives higher w near the lower r
                larger du gives higher w near the higher r
    Output:
        w       weights
    """

    # distance from nesting nodes to the reference polyline
    line = geom.MultiLineString(mgeom.poly_nan2list(np.vstack((xb,yb)).T))
    d = np.array([line.distance(geom.Point(xk,yk)) for xk,yk in zip(xn,yn)])
    
    r = d/rwidth
    if linear:
        w = r.copy()
    else:
        w = (np.tanh((du+dl)*r-dl) + np.tanh(dl))/(np.tanh(du) + np.tanh(dl))
        
    w[r>1] = 1 # ones beyond rwidth and to the outer boundary of the nesting zone
           
    return w

    
def nemo_nesting_zone(xn,yn,xf,yf):
    """ NEMO nodes in the nesting zone.
    Indices of NEMO nodes corresponding to the fvcom nesting nodes.
    Input:
        xn,yn   (ny,nx) 2D arrays, UTM coords of nemo nodes
        xf,xf   UTM coords of nesting zone nodes
    Output:
        locn    linear (raveled) indices into nemo grid
    """
    
    xn = np.ravel(xn)
    yn = np.ravel(yn)
    
    tol = 2. # match with tolerance (in metres)
    
    # pairwise distance matrix; NB: Prone to memory overflows for large arrays
    try: # cdist is prone to memory overflow for large arrays
        pd = scipy.spatial.distance.cdist(np.c_[xf,yf],np.c_[xn,yn])
        locf,locn = np.where(pd<tol)
    except MemoryError:
        # do in chunks
        mem_avail = psutil.virtual_memory().available # bytes
        mem_need = xf.size * xn.size * np.max([xf.itemsize,xn.itemsize]) # bytes
        nchunks = np.ceil(mem_need/mem_avail/0.5).astype(int) # request no more than 50% of memory
        chunk = np.ceil(xn.size/nchunks).astype(int) # number of elements from xn,yn per chunk
        locf,locn = np.array([],dtype=int), np.array([],dtype=int)
        xyf = np.c_[xf,yf]
        xyn = np.c_[xn,yn]
        for k in range(nchunks):
            pd = scipy.spatial.distance.cdist(xyf,xyn[k*chunk:(k+1)*chunk,:])
            locfk,locnk = np.where(pd<tol)
            pd = None # free up memory
            locf = np.r_[locf,locfk] # extend arrays
            locn = np.r_[locn,locnk+k*chunk] # increase indices according to the chunk
        
        isort = np.argsort(locf)
        locn = locn[isort]
        
#        # for a check
#        locn = np.zeros(xf.shape,dtype=int)
#        for k in range(len(xf)):
#            pd = ((xf[k]-xn)**2 + (yf[k]-yn)**2)**.5
#            locnk = np.where(pd<tol)
#            locn[k] = locnk[0]
    
    if len(locn)<len(xf):
        raise ValueError('Not all nesting zone nodes have been accounted for.')
    
    return locn
        

def nemo_azimuth(lon,lat,utmzone):
    """ NENO grid rotation relative to UTM projection.
    Local angle between nemo grid and UTM northing direction (fvcom grid 
    coordinate system).
    
    Input:
        lon,lat     (ny,nx) 2D arrays of coordinates for the NEMO grid
        utmzone     UTM zone used for FVCOM grid (numerical value)
        
    Output:
        theta       (ny,nx) local angle in radians CW from north (east of north)
    """
    utmproj = Proj("+proj=utm +zone="+str(utmzone))
    x,y = utmproj(lon,lat)
    
    theta = np.arctan2(np.diff(x,axis=0),np.diff(y,axis=0)) # radians, CW from northing
    theta = np.r_['0,2',theta,theta[-1,:]] # copy the last row to preserve the size of the grid
    
#    np.savetxt('nemo_azimuth_2.txt',theta) # checked: good

    return theta


def uv_interpolant(locz,nemo_grid_shape,dx,dy,ifnz,tri):
    """ Interpolant to interpolate from nemo U- and V-grid to fvcom centroids in
    the nesting zone.
    The fvcom nesting zone nodes must match locations of the nemo nodes.
    Assuming nemo grid is regular and rectangular.
    
    Input:
        locz    linear (raveled) indices of NEMO nodes in the nesting zone
        nemo_grid_shape     (ny,nx)
        dx,dy   NEMO grid spacing in x- and y-direction (e1t,e2t variables in the 
                nemo coordinates file); assuming grid spacing for u- and v-grids 
                is very close to that for t-grid
        ifnz    indices of fvcom nodes in the nesting zone (0-based indices)
        tri     (nelem,3) triangulation array for fvcom grid (1-based indices)
        
    Output:
        locu,wu,locv,wv     linear indices into NEMO u- and v-grids and corresponding weights
    """
    
    j,i = np.unravel_index(locz, nemo_grid_shape)
#    np.savetxt('nemo_nesting_zone_cut_2.txt', np.c_[j,i]) # checked: good
    
    # fvcom elements in the nesting zone
    [lia,locb] = mut.ismember(tri,ifnz+1) # locb contains indices to nesting zone list
    itrin = np.all(lia,axis=1) # triangles containg all three nesting zone nodes
    trin = locb[itrin,:].astype(int) # 0-based; keep only nesting zone triangles
    it = i[trin] # nemo i-indices for nesting zone triangles
    jt = j[trin] # nemo j-indices for nesting zone triangles
    # it,jt contain two identical indices in each row, since fvcom triangles
    # were built from rectangular nemo cells, two triangles per cell.
    
    # figure out nemo node indices for interpolant
    ib = np.min(it,axis=1) # "base" index, closest i-index of u-grid
    jb = np.min(jt,axis=1) # "base" index, closest j-index of v-grid
    imf = np.array([np.bincount(row).argmax() for row in it]) # most frequent element in each row
    jmf = np.array([np.bincount(row).argmax() for row in jt]) # most frequent element in each row
    im = np.mean(it,axis=1)
    jm = np.mean(jt,axis=1)
    si = np.sign(im-imf) # -1 or 1
    sj = np.sign(jm-jmf) # -1 or 1
    ilf = imf + si # least frequent
    jlf = jmf + sj # least frequent
    # figure;plot(imf-ilf,'.') % check that this is either -1 or 1
    
    # interpolant for u; nemo u-grid nodes
    iu = np.c_[ib,  ib,  ib+si, ib+si]
    ju = np.c_[jmf, jlf, jmf,   jlf]
    
    # interpolant for v; nemo v-grid nodes
    iv = np.c_[imf, ilf, imf,   ilf]
    jv = np.c_[jb,  jb,  jb+sj, jb+sj]
    
    # use same grid spacing for neighbouring cells: difference is within 3-4 cm
    iravel = np.ravel_multi_index((jb,ib), dims=dx.shape) # dy.shape is the same
    dx = np.take(dx,iravel)
    dy = np.take(dy,iravel)
    
    # distances
    wu = np.array([(1/6*dx)**2 + (1/3*dy)**2,  (1/6*dx)**2 + (2/3*dy)**2,  
                   (5/6*dx)**2 + (1/3*dy)**2,  (5/6*dx)**2 + (2/3*dy)**2])**.5
    wv = np.array([(1/6*dy)**2 + (1/3*dx)**2,  (1/6*dy)**2 + (2/3*dx)**2,  
                   (5/6*dy)**2 + (1/3*dx)**2,  (5/6*dy)**2 + (2/3*dx)**2])**.5
    wu = wu.T.copy()
    wv = wv.T.copy()
    
    # weights
    wu = 1/wu # inverse distances
    wu = wu/np.sum(wu,axis=1,keepdims=True) # normalize rows to 1
    wv = 1/wv
    wv = wv/np.sum(wv,axis=1,keepdims=True)
    #% % for uniformly spaced square grid, the weights would be
    #% w = [(1/6)**2 + (1/3)**2  (1/6)**2 + (2/3)**2  (5/6)**2 + (1/3)**2  (5/6)**2 + (2/3)**2]**.5;
    #% w = 1./w;
    #% w = w/sum(w);
    #% w =    0.4335    0.2351    0.1800    0.1514
    
    locu = np.ravel_multi_index((ju.astype(int),iu.astype(int)), dims=nemo_grid_shape)
    locv = np.ravel_multi_index((jv.astype(int),iv.astype(int)), dims=nemo_grid_shape)

    # checked: good
#    np.savetxt('interpolant_indices_u_i_2.txt', iu.astype(int), fmt='%d')
#    np.savetxt('interpolant_indices_u_j_2.txt', ju.astype(int), fmt='%d')
#    np.savetxt('interpolant_indices_v_i_2.txt', iv.astype(int), fmt='%d')
#    np.savetxt('interpolant_indices_v_j_2.txt', jv.astype(int), fmt='%d')
#    np.savetxt('interpolant_indices_u_w_2.txt', wu)
#    np.savetxt('interpolant_indices_v_w_2.txt', wv)
    
    return locu,wu,locv,wv


def nemo_vertical_weight(e3u_0,e3v_0,umask,vmask):
    """ Weights for vertical averaging of NEMO velocities.
    Weights for layers below the bottom are zeroed out.
    
    Input:
        e3u_0,e3v_0 (layers, y, x) vertical spacing of U- and V-grids
        umask,vmask (layers, y, x) corresponding masks with zeros below the bottom
    
    Output:
        vwu,vwv     weights for vertical layers
    """
    
    with np.errstate(invalid='ignore'): # suppress division by zero warning
        vwu = e3u_0.copy()
        vwu[umask==0] = 0
        vwu = vwu/np.sum(vwu,axis=0)
        vwu[np.isnan(vwu)] = 0
        
        vwv = e3v_0.copy()
        vwv[vmask==0] = 0
        vwv = vwv/np.sum(vwv,axis=0)
        vwv[np.isnan(vwv)] = 0
    
    return vwu,vwv


def generate_type3_nesting_file(fout,
                            node_w, node_lon, node_lat, node_x, node_y, dep_noden,
                            cell_w, cell_lon, cell_lat, cell_x, cell_y, dep_elemn,
                            iz,iu,iuw,iv,ivw,
                            wu,wv, theta,
                            nsiglev, siglev, nsiglay, siglay,
                            nemo_file_list, 
                            time_start = None, time_end = None,
                            opt = 'BRTP', nrampup = 0, TBRTP = 10.0, SBRTP = 35.0,
                            gdeptn=None,usub=None,vsub=None,
                            ivt1=None,ivt2=None,wvt1=None,wvt2=None,
                            ivu1=None,ivu2=None,wvu1=None,wvu2=None,
                            ivv1=None,ivv2=None,wvv1=None,wvv2=None,
                            ivw1=None,ivw2=None,wvw1=None,wvw2=None,
                            ua_name=None,va_name=None,
                            u_name=None,v_name=None,w_name=None,
                            t_name=None,s_name=None):
    """ Generation of FVCOM Type 3 nesting file from a structured grid z-level model (NEMO) output.
    FVCOM nodes in the nesting zone should be matched with NEMO nodes.
    Time step for the forcing series corresponds to the NEMO output time step.
    Inputs:
        fout                str, nesting file name
        node_w              weights for the nesting zone nodes
        node_lon,node_lat,node_x,node_y,dep_noden   node coordinates and depths
        cell_w              weights for the nesting zone elements
        cell_lon,cell_lat,cell_x,cell_y,dep_elemn   element coordinates and depths
        iz                  indices (raveled) into NEMO grid for the nodes in the nesting zone
        iu,iuw,iv,ivw       indices (raveled) and weights to interpolate U,V from 
                            NEMO to FVCOM elements in the nesting zone
        wu,wv               weights for vertical averaging of NEMO velocities
        theta               rotation angle between NEMO and FVCOM grids
        nsiglev,siglev,nsiglay,siglay   FVCOM sigma-layers and -levels
                                        (no support for hybrid coordinate option)
        nemo_file_list      list of "T" NEMO output files; 
                            U,V,W file names must differ only in the 4th character from the end
        time_start,time_end     str, FVCOM simulation start and end times in 
                                '%Y-%m-%d %H:%M:%S' format
        opt                 'BRTP' (default) or 'BRCL', barotropic or baroclinic
        nrampup             number of time steps for FVCOM ramp-up (in time steps of NEMO output)
                            default is 0
        TBRTP,SBRTP         fill-in values for T,S for barotropic case
        gdeptn              T-grid NEMO layer depths at the nodes in the nesting zone
        usub,vsub           indices for extending u,v values below the bottom for 
                            subsequent interpolation to fvcom elements
        ivt1,ivt2,wvt1,wvt2     indices and weights to interpolate t,s,u,v to 
        ivu1,ivu2,wvu1,wvu2     fvcom sigma-layers and w to sigma-levels
        ivv1,ivv2,wvv1,wvv2
        ivw1,ivw2,wvw1,wvw2
        ua_name,va_name     variable names for vertically averged velocity 
                            components if they are present in NEMO output files
        u_name,v_name,w_name    variable names for velocity components if 
                                different from standard NEMO names
        t_name,s_name       variable names for temperature and salinity if 
                            different from standard NEMO names
    """
    
    if u_name is None: u_name='vozocrtx'
    if v_name is None: v_name='vomecrty'
    if w_name is None: w_name='vovecrtz'
    if t_name is None: t_name='votemper'
    if s_name is None: s_name='vosaline'
                            
    nemo_time, nemo_file, nemo_kt = nemo_timeseries(nemo_file_list) # U,V must have same time

    if time_start is not None and time_end is not None:
        # Only process nemo times between time_start and time_end
        startt = datetime.datetime.strptime(time_start, '%Y-%m-%d %H:%M:%S')
        endt = datetime.datetime.strptime(time_end, '%Y-%m-%d %H:%M:%S')
        idx = np.asarray([i for i in range(len(nemo_time)) if nemo_time[i] >= startt and nemo_time[i] <= endt])
        nemo_time = [nemo_time[i] for i in idx]
        nemo_file = [nemo_file[i] for i in idx]
        nemo_kt   = [nemo_kt[i]   for i in idx]

    ntimes = np.size(nemo_time)
    print('ntimes = {}'.format(ntimes))

    num_nest_nodes = len(node_x)
    num_nest_elems = len(cell_x)
    print('ncells = {}'.format(num_nest_elems))
    print('nnodes = {}'.format(num_nest_nodes))

    print('Number of sigma layers = {}'.format(nsiglay))
    
    # expand weights to time dimension and ramp-up linearly for nrampup values
    cell_wt = rampup_weights(cell_w,ntimes,nrampup)
    node_wt = rampup_weights(node_w,ntimes,nrampup)
    
    # Write nesting file
    ncfile = setup_nesting_ncfile(fout, num_nest_elems, num_nest_nodes, nsiglay, nsiglev)
    fvt.write_time(ncfile, nemo_time)
    print("Finished writing times ...")
    
    ncfile.variables['lon'][:] = node_lon
    ncfile.variables['lat'][:] = node_lat
    ncfile.variables['lonc'][:] = cell_lon
    ncfile.variables['latc'][:] = cell_lat
    ncfile.variables['x'][:] = node_x
    ncfile.variables['y'][:] = node_y
    ncfile.variables['xc'][:] = cell_x
    ncfile.variables['yc'][:] = cell_y
    
#    h,h_center,siglay,siglay_center # for FVCOM4
    ncfile.variables['h'][:] = dep_noden
    ncfile.variables['h_center'][:] = dep_elemn
    ncfile.variables['siglay'][:,:] = np.repeat(siglay[:,np.newaxis], num_nest_nodes, axis=1) # siglay x node
    ncfile.variables['siglay_center'][:,:] = np.repeat(siglay[:,np.newaxis], num_nest_elems, axis=1) # siglay x nele
    ncfile.variables['siglev'][:,:] = np.repeat(siglev[:,np.newaxis], num_nest_nodes, axis=1) # siglev x node
    
    ncfile.variables['weight_cell'][:,:] = cell_wt
    ncfile.variables['weight_node'][:,:] = node_wt
                    
    if opt == 'BRTP':
        zeros_tln = np.zeros((ntimes, nsiglay, num_nest_nodes), dtype = 'float')
        tsiglay = zeros_tln + TBRTP
        ssiglay = zeros_tln + SBRTP
        wsiglay = np.zeros((ntimes, nsiglev, num_nest_nodes), dtype = 'float')
        ncfile.variables['temp'][:,:,:] = tsiglay
        ncfile.variables['salinity'][:,:,:] = ssiglay
        ncfile.variables['hyw'][:,:,:] = wsiglay
    
    # iterate through the time series
    for ntime, fname, k, kt in zip(nemo_time, nemo_file, nemo_kt, range(ntimes)):
        print('Working on time {}'.format(ntime))
        
        ncfilet = nc.Dataset(fname)
        # extract entire field for k-th time step in file
        z = ncfilet.variables['sossheig'][k,::]
        # Dimensions: x,y,time_counter. In python, nc dimensions go backwards.
        
        ncfileu = nc.Dataset(fname[:-4]+'U.nc')
        ncfilev = nc.Dataset(fname[:-4]+'V.nc')
        
        
        if ua_name is not None and va_name is not None: 
            # read vertically averged velocity if present in NEMO files
            ua = ncfileu.variables[ua_name][k,:,:] # float32,(time_counter,y,x)
            va = ncfilev.variables[va_name][k,:,:]
        else: 
            # otherwise, read velocity and averge vertically
            u = ncfileu.variables[u_name][k,:,:,:] # Dimensions: (time_counter,depth,y,x)
            v = ncfilev.variables[v_name][k,:,:,:]
            
            # average U,V vertically
            ua = np.einsum('kji,kji->ji',u,wu)
            va = np.einsum('kji,kji->ji',v,wv)
        
        # rotate to fvcom coordinate system
        ua,va = fvt.rotate(ua,va,-theta,degrees=False)
        
        # interpolate to fvcom nesting zone locations (still on nemo z-levels)
        zf = np.take(z,iz) # ssh at fvcom nesting zone nodes
        uaf = np.sum( np.take(ua,iu) * iuw, axis=1)
        vaf = np.sum( np.take(va,iv) * ivw, axis=1)
        
        if opt == 'BRTP':
            # replicate averaged velocities to all sigma-layers
            usiglay = np.tile(uaf[np.newaxis,:],(nsiglay,1))
            vsiglay = np.tile(uaf[np.newaxis,:],(nsiglay,1))
            
        elif opt == 'BRCL':
            nzsl = u.shape[0] # number of nemo z-levels
            
            ct = ncfilet.variables[t_name][k,:,:,:]
            sr = ncfilet.variables[s_name][k,:,:,:]
            ncfilew = nc.Dataset(fname[:-4]+'W.nc')
            w = ncfilew.variables[w_name][k,:,:,:]
            
            # pick t,s,w in fvcom nesting zone
            ct = np.take(ct.reshape((nzsl,-1)),iz,axis=1) # at fvcom nesting zone nodes
            sr = np.take(sr.reshape((nzsl,-1)),iz,axis=1)
            wf = np.take( w.reshape((nzsl,-1)),iz,axis=1)
            
            # convert Conservative Temperature, Reference Salinity to 
            # in-situ temperature, Practical Salinity
            # supply negative layer depths
            tf,sf = CtSr2TS(ct,sr,-gdeptn,dep_noden,zf,node_lon,node_lat)
            
            # read velocity if wasn't read earlier
            if ua_name is None or va_name is None:
                u = ncfileu.variables[u_name][k,:,:,:]
                v = ncfilev.variables[v_name][k,:,:,:]
            
            # rotate to fvcom coordinate system
            u,v = fvt.rotate(u,v,-theta,degrees=False)
            
            # interpolate u,v to fvcom nesting zone
            u = np.take(u,usub) # extend below the bottom
            v = np.take(v,vsub)
            uf = np.sum( np.take(u.reshape((nzsl,-1)),iu,axis=1) * iuw, axis=2)
            vf = np.sum( np.take(v.reshape((nzsl,-1)),iv,axis=1) * ivw, axis=2)
            
            # interpolate t,s,u,v to fvcom sigma-layers, w to sigma-levels
            tsiglay = np.take(tf,ivt1)*wvt1 + np.take(tf,ivt2)*wvt2
            ssiglay = np.take(sf,ivt1)*wvt1 + np.take(sf,ivt2)*wvt2
            usiglay = np.take(uf,ivu1)*wvu1 + np.take(uf,ivu2)*wvu2
            vsiglay = np.take(vf,ivv1)*wvv1 + np.take(vf,ivv2)*wvv2
            wsiglev = np.take(wf,ivw1)*wvw1 + np.take(wf,ivw2)*wvw2
            
            ncfile.variables['temp'][kt,:,:] = tsiglay
            ncfile.variables['salinity'][kt,:,:] = ssiglay
            ncfile.variables['hyw'][kt,:,:] = wsiglev
        
        ncfile.variables['ua'][kt,:] = uaf
        ncfile.variables['va'][kt,:] = vaf
        ncfile.variables['u'][kt,:,:] = usiglay
        ncfile.variables['v'][kt,:,:] = vsiglay        
        ncfile.variables['zeta'][kt,:] = zf
    
    ncfile.close()
    

def rampup_weights(w,ntimes,nrampup):
    """
    Expand weights to time dimension and ramp-up linearly from 0 to 1 for the leading nrampup values.
    Returned weights are in [time,node] shape.
    """
    n1 = max(ntimes-nrampup,0) # number of time stamps after ramp-up
    ramp = np.hstack([np.linspace(0,1,nrampup),np.ones(n1)]) # ramping    
    ramp = ramp[:ntimes] # trim in case the series is shorter than the ramp-up time
    wt = np.outer(ramp,w) # matrix multiplication for vectors: column ramp by row w
    return wt


def list_nemo_files(folder,pattern,varname):
    """List NEMO output files.
    List all NEMO output files for the specified varaible in the 
    specified folder and all subfolders recursively according to search pattern.
    
    Inputs:
        folder      str or list, a single folder or a list of folders where 
                    to search for NEMO output files
        pattern     str, file serach pattern to identify NEMO output files,
                    e.g. pattern='SalishSea_1h_*_grid_' for the full SSC grid;
                    assuming files end in varname+'.nc', e.g. ...T.nc.
                    Alternatively, pattern for full name, including '.nc'.
        varname     optional, 'T', 'U', 'V'. Appended to pattern if partial 
                    parttern is specified.
    """
    if len(pattern) > 2 and pattern[-3:] != '.nc':
        pattern = pattern + varname + '.nc'
        
    print('list_nemo_files: search folder:')
    print(folder)
    
    if type(folder) is str: folder = [folder] # ensure it's a list
    
    # Code from:
    # https://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
    matches = []
    
    for one_folder in folder:
        for root, dirnames, filenames in os.walk(one_folder):
            for filename in fnmatch.filter(filenames, pattern):
                matches.append(os.path.join(root, filename))
            
    print('list_nemo_files: file list:')
    print(matches)
    
    return matches


#def nemo_nctime(fname):
#    """Read time stamps from a NEMO output file.
#    Return time as a list of datetime objects.
#    """
#    ncfile = nc.Dataset(fname)
##    ntimes = len( ncfile.dimensions['time_counter'] )
#    timevar = ncfile.variables['time_counter']
#    if timevar.getncattr('units') != 'seconds since 1900-01-01 00:00:00':
#        raise RuntimeError('Unknown units in file: '+fname)
#    else:
#        t0 = datetime.datetime(1900,1,1,0,0,0)
#        t = [t0 + datetime.timedelta(seconds = tk) for tk in timevar[:]]
#        return t

def nemo_nctime(fname):
    """ Get time from a NEMO output file.
    
    Parameters
    ----------
    fname : str
        File name
    
    Returns
    -------
    time : array_like of datetime objects
    """
    with nc.Dataset(fname) as ncf:
        timec = ncf.variables['time_counter'][:].filled()
        units,torigin = ncf.variables['time_counter'].getncattr('units').split(' since ')
    t0 = datetime.datetime.strptime(torigin,'%Y-%m-%d %H:%M:%S')
    # treat both array nad scalar cases
    try:
        _ = iter(timec)
    except TypeError:
        # not iterable
        time = t0 + datetime.timedelta(**{units:timec})
    else:
        # iterable
        time = np.array([t0 + datetime.timedelta(**{units:timec1}) for timec1 in timec])
    
    return time
    

def nemo_timeseries(filelist):
    """Time series for NEMO output with file names and time indices within each file.
    """
    
    # collect all time stamps from all files together with corresponding 
    # file names, time indices, and forecast base times
    t = [] # to keep time steps
    f = [] # to keep list of file names for each time stamp
    kt = [] # to keep time index of the corresponding time stamp in the file
    ti = [] # to keep initial time for each file (for each time stamp)
    for fname in filelist:
#        t.append(nemo_nctime(fname)) # needs numpy 1.13?
        tf = nemo_nctime(fname)
        nt = len(tf) # number of time stamps in the file
        t.extend(tf) # append to the global list
        f.extend([fname]*nt) # replicate file name nt times and append to global list
        kt.extend(range(nt)) # time indices (zero-based)
        ti.extend([tf[0]]*nt) # replicate initial time nt times and append to global list
    
#    # sort all lists by time
#    isort = np.argsort(t)
#    t = [t[i] for i in isort]
#    f = [f[i] for i in isort]
#    kt = [kt[i] for i in isort]
#    ti = [ti[i] for i in isort]
#    
#    # exclude duplicates: choose by the latest initial time (would be the latest forecast)
#    ts = [tk.timestamp() for tk in t]
#    tsu,ui,uui,c = np.unique(ts, return_index=True, return_inverse=True, return_counts=True)
#    idup = np.where(c>1)[0]
#    ndup = len(idup)
#    if ndup>0:
#        print('nemo_timeseries: {} time stamps are duplicated. Removing duplicates.'.format(ndup))
#        tis = np.array([tk.timestamp() for tk in ti])
#        for k in range(ndup):
#            irep = np.where(uui==idup[k])[0] # all duplicates for this time step
#            isub = np.argmax(tis[irep]) # select latest initial time
#                # returns a single index, even if duplicates have same initial time
#            ui[idup[k]] = irep[isub] # substitute index 
#        # select unique times
#        t = [t[i] for i in ui]
#        f = [f[i] for i in ui]
#        kt = [kt[i] for i in ui]
    
    # exclude duplicates: np.unique chooses the values which occur first in list
    ts = [tk.timestamp() for tk in t]
    tsu,ui = np.unique(ts, return_index=True) # returns list sorted by time
    # select unique times
    t = [t[i] for i in ui]
    f = [f[i] for i in ui]
    kt = [kt[i] for i in ui]
    
    return t, f, kt


def setup_nesting_ncfile(fnc, nele, nnode, nsiglay, nsiglev):
    ncfile = nc.Dataset(fnc, mode = 'w', format = 'NETCDF3_CLASSIC')
    
    fvt.setup_time(ncfile)
    
    ncfile.createDimension('nele',nele)
    ncfile.createDimension('node',nnode)
    ncfile.createDimension('siglay',nsiglay)
    ncfile.createDimension('siglev',nsiglev)
    ncfile.createDimension('three',3)
    #~ ====================================================================================
    data=ncfile.createVariable('lon','float32', ('node',) )
    setattr(data,'long_name','nodal longitude')
    setattr(data,'standard_name','longitude')
    setattr(data,'units','degrees_east')
    #~ ====================================================================================
    data=ncfile.createVariable('lat','float32',('node',))
    setattr(data,'long_name','nodal latitude')
    setattr(data,'standard_name','latitude')
    setattr(data,'units','degrees_north')
    #~ ====================================================================================
    data=ncfile.createVariable('lonc','float32',('nele',))
    setattr(data,'long_name','zonal longitude')
    setattr(data,'standard_name','longitude')
    setattr(data,'units','degrees_east')
    #~ ====================================================================================
    data=ncfile.createVariable('latc','float32',('nele',))
    setattr(data,'long_name','zonal latitude')
    setattr(data,'standard_name','latitude')
    setattr(data,'units','degrees_north')
    #~ ====================================================================================
    data=ncfile.createVariable('x','float32', ('node',) )
    setattr(data,'long_name','nodal x-coordinate')
    setattr(data,'units','meters')
    #~ ====================================================================================
    data=ncfile.createVariable('y','float32',('node',))
    setattr(data,'long_name','nodal y-coordinate')
    setattr(data,'units','meters')
    #~ ====================================================================================
    data=ncfile.createVariable('xc','float32',('nele',))
    setattr(data,'long_name','zonal x-coordinate')
    setattr(data,'units','meters')
    #~ ====================================================================================
    data=ncfile.createVariable('yc','float32',('nele',))
    setattr(data,'long_name','zonal y-coordinate')
    setattr(data,'units','meters')
    #~ ====================================================================================
    data=ncfile.createVariable('siglay','float32',('siglay','node',))
    setattr(data,'long_name','Sigma Layers')
    setattr(data,'standard_name','ocean_sigma/general_coordinate')
    setattr(data,'positive','up')
    setattr(data,'valid_min',-1.0)
    setattr(data,'valid_max', 0.0)
    setattr(data,'formula_terms', 'sigma: siglay eta: zeta depth: h')
    #~ ====================================================================================
    data=ncfile.createVariable('siglay_center','float32',('siglay','nele',))
    setattr(data,'long_name','Sigma Layers at elements') #MK: invented it myself
    setattr(data,'standard_name','ocean_sigma/general_coordinate')
    setattr(data,'positive','up')
    setattr(data,'valid_min',-1.0)
    setattr(data,'valid_max', 0.0)
    setattr(data,'formula_terms', 'sigma: siglay eta: zeta depth: h')
    #~ ====================================================================================
    data=ncfile.createVariable('siglev','float32',('siglev','node',))
    setattr(data,'long_name','Sigma Levels')
    setattr(data,'standard_name','ocean_sigma/general_coordinate')
    setattr(data,'positive','up')
    setattr(data,'valid_min',-1.0)
    setattr(data,'valid_max', 0.0)
    setattr(data,'formula_terms', 'sigma: siglay eta: zeta depth: h')
    #~ ====================================================================================
    data=ncfile.createVariable('h','float32',('node',))
    setattr(data,'long_name','Bathymetry')
    setattr(data,'standard_name','sea_floor_depth_below_geoid')
    setattr(data,'units','m')
    setattr(data,'positive','down')
    setattr(data,'grid','Bathymetry_Mesh')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('h_center','float32',('nele',))
    setattr(data,'long_name','Bathymetry at elements') #MK: invented it myself
    setattr(data,'standard_name','sea_floor_depth_below_geoid')
    setattr(data,'units','m')
    setattr(data,'positive','down')
    setattr(data,'grid','fvcom_grid') #MK: invented it myself
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')    
    #~ ====================================================================================
    #TODO: check if it's used in fvcom, because nothing is written to nv in this code
    data=ncfile.createVariable('nv','int32',('three','nele',))
    setattr(data,'long_name','nodes surrounding element')
    #~ ====================================================================================
    data=ncfile.createVariable('zeta','float32',('time','node',))
    setattr(data,'long_name','Water Surface Elevation')
    setattr(data,'standard_name','sea_surface_elevation')
    setattr(data,'units','meters')
    setattr(data,'positive','up')
    setattr(data,'grid','SSH_Mesh')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('ua','float32',('time','nele',))
    setattr(data,'long_name','Vertically Averaged x-velocity')
    setattr(data,'units','meters s-1')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('va','float32',('time','nele',))
    setattr(data,'long_name','Vertically Averaged y-velocity')
    setattr(data,'units','meters s-1')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('u','float32',('time','siglay','nele',))
    setattr(data,'long_name','Eastward Water Velocity')
    setattr(data,'units','meters s-1')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('v','float32',('time','siglay','nele',))
    setattr(data,'long_name','Northward Water Velocity')
    setattr(data,'units','meters s-1')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('temp','float32',('time','siglay','node',))
    setattr(data,'long_name','temperature')
    setattr(data,'standard_name','sea_water_temperature')
    setattr(data,'units','degrees_C')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('salinity','float32',('time','siglay','node',))
    setattr(data,'long_name','salinity')
    setattr(data,'standard_name','sea_water_salinity')
    setattr(data,'units','1 e-3')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('hyw','float32',('time','siglev','node',))
    setattr(data,'long_name','hydrostatic vertical velocity')
    setattr(data,'units','m/s')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('weight_node','float32',('time','node',))
    setattr(data,'long_name','weight of node')
    setattr(data,'units','-')
    setattr(data,'grid','SSH_mesh')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    #~ ====================================================================================
    data=ncfile.createVariable('weight_cell','float32',('time','nele',))
    setattr(data,'long_name','weight of cell')
    setattr(data,'units','-')
    setattr(data,'grid','fvcom_grid')
    setattr(data,'coordinates','x y')
    setattr(data,'type','data')
    
    return ncfile


def make_its(fout, x,y,tri,utmzone, 
             nemo_lon, nemo_lat, dx,dy, tmask, gdept_0, nemo_h, gdept_1d,
             nemo_file_list, time_start, 
             t_name=None,s_name=None,file_exclude_poly=None,do_conversion=True):
    """ Create FVCOM initial TS conditions file from NEMO output.
    
    Inputs:
        fout                  str   name of the its file to create
        x,y,tri                     fvcom grid
        utmzone               int   UTM zone used for FVCOM grid
        nemo_lon,nemo_lat   (y,x)   nemo T-grid coordinates
        dx,dy               (y,x)   x- and y-spacing of nemo T-grid (e1t,e2t in nemo metrics)
        tmask   (nemo_layers,y,x)   from the NEMO mask file
        gdept_0 (nemo_layers,y,x)   depths of layers at zero sea level elevation
        nemo_h              (y,x)   bathymetry at nodes; positive down, m
        gdept_1d    (nemo_layers)   nominal depth of T-grid points (t,z)
        nemo_file_list       list   NEMO output files from where to take T,S,zeta 
        time_start            str   fvcom start time in '%Y-%m-%d %H:%M:%S' format
        t_name,s_name         str   T,S variable names if different from standard NEMO names
        file_exclude_poly     str   name of the file containing coordinates of the 
                                    polygon where the values interpolated from nemo grid 
                                    should be invalidated before extrapolation;
                                    the polygon can contain several nan-separated loops;
                                    coordinates are the 1st two columns, comma-separated;
                                    coordinates are in the same system as the fvcom grid
        do_conversion        bool   convert Conservative Temperature, Reference Salinity 
                                    to in-situ temperature, Practical Salinity
    """
    t,s = its_from_nemo(x,y,tri,utmzone, 
                        nemo_lon, nemo_lat, dx,dy, tmask, gdept_0, nemo_h,
                        nemo_file_list, time_start, 
                        t_name=t_name,s_name=s_name,
                        file_exclude_poly=file_exclude_poly,
                        do_conversion=do_conversion)
    
    # Since nearest neighbour extrapolation to deep layers is used in its_from_nemo, 
    # we can use gdept_1d as zsl.
    # NOTE: zsl in fvcom ITS file should be negative; take -gdept_1d;
    # supply values in (layers,nodes) shape
    fvt.make_observed_its(fout, time_start, -gdept_1d, t.T, s.T)
    
    
def its_from_nemo(x,y,tri,utmzone, 
                  nemo_lon, nemo_lat, dx,dy, tmask, gdept_0, nemo_h,
                  nemo_file_list, time_start, 
                  t_name=None,s_name=None,file_exclude_poly=None,do_conversion=True):
    """ Get TS at FVCOM nodes for the specified time from NEMO output.
    """
    
    if t_name is None: t_name='votemper'
    if s_name is None: s_name='vosaline'
    
    nemo_wtmp,nemo_sals,zeta = nemo_ts_snapshot(nemo_file_list,
                                                time_start,t_name,s_name)
    
    # convert Conservative Temperature, Reference Salinity to in-situ temperature, Practical Salinity
    if do_conversion:
        # supply negative gdept_0
        nemo_wtmp,nemo_sals = CtSr2TS(nemo_wtmp,nemo_sals,-gdept_0,nemo_h,zeta,nemo_lon,nemo_lat)
    
    # sub values below bottom with the deepest valid value for correct 
    # application of uv interpolant
    tsub = below_bottom_sub(tmask)
    # extend below the bottom
    nemo_wtmp = np.take(nemo_wtmp,tsub)
    nemo_sals = np.take(nemo_sals,tsub)
    
    
    print("Interpolating to fvcom nodes ...")
    if utmzone is None: # fvcom coords are lon,lat
        utmzone = best_utm_zone(x)
        convert_fvcom_coords = True
    else:
        convert_fvcom_coords = False

    utmproj = Proj("+proj=utm +zone="+str(utmzone))
    nemo_x,nemo_y = utmproj(nemo_lon,nemo_lat)
    if convert_fvcom_coords:
        x,y = utmproj(x,y)
 
    # invalidate land nodes based on mask for surface layer; 
    # all profiles will be extended to the deepest layer subsequently
    nemo_x[tmask[0,:,:]==0] = np.inf
    
    fvcom_xy = np.hstack((x[:,None],y[:,None]))
    nemo_xy  = np.hstack((nemo_x.ravel()[:,None],nemo_y.ravel()[:,None]))
    # diagonal corner-to-corner distance in each nemo cell to use as a threshold 
    # for fvcom to nemo node distance
    r = ((dx**2+dy**2)**.5).ravel()
    loc,w = interp.interpolant2d(fvcom_xy,nemo_xy,r,method='kdtree')
    
    # interpolate t,s from nemo grid to fvcom nodes
    nzsl = nemo_wtmp.shape[0] # number of nemo z-layers
    t_intp = np.sum( np.take(nemo_wtmp.reshape((nzsl,-1)),loc,axis=1) * w, axis=2)
    s_intp = np.sum( np.take(nemo_sals.reshape((nzsl,-1)),loc,axis=1) * w, axis=2)
    print("Finished interpolating to fvcom nodes ...")
    
    # invalidate specified areas in fvcom grid
    if file_exclude_poly is not None:
        xypoly = np.genfromtxt(file_exclude_poly,delimiter=',',usecols=(0,1))
        inp = mgeom.inpoly(x,y,xypoly)
        t_intp[:,inp] = np.nan
        s_intp[:,inp] = np.nan
    
    # Fill in the missing values and smooth the filled part (see ts_intp_fill_and_smooth.m)
    ifix = ~np.isnan(t_intp[0,:]) # nans are same for all layers, same for t and s
    tf = mesh.fill_laplacian(tri,t_intp.T) # values to (nodes,layers) shape
    sf = mesh.fill_laplacian(tri,s_intp.T)
    ts = mesh.smooth_laplacian(tri,tf,ifix=ifix)
    ss = mesh.smooth_laplacian(tri,sf,ifix=ifix)
    
    return ts,ss


def best_utm_zone(lon):
    """ Determine the most appropriate UTM zone based on central meridian for the data.
    """
    lomin, lomax = lon.min(), lon.max()
    lonc = lomin + 0.5*(lomax-lomin) # central meridian
    if lonc > 180: lonc-=360
    return np.fix(lonc/6 + 31)


def nemo_ts_snapshot(nemo_file_list, snap_time, 
                     t_name='votemper', s_name='vosaline'):
    """ NEMO T,S for specified time.
    Interpolates between two nemo times if the specified time doesn't match any 
    nemo time stamp.
    """
    
    tsnap = datetime.datetime.strptime(snap_time,'%Y-%m-%d %H:%M:%S')
    
    nemo_time, nemo_file, nemo_kt = nemo_timeseries(nemo_file_list)
    
    i1,i2,w1,w2 = interp.interpolant1d(date2num(tsnap), date2num(nemo_time))
    
    # take data for 1st time stamp
    ncfile = nc.Dataset(nemo_file[i1])
    nemo_wtmp = ncfile.variables[t_name][nemo_kt[i1],:,:,:] # (layer,y,x)
    nemo_sals = ncfile.variables[s_name][nemo_kt[i1],:,:,:]
    if 'sossheig' in ncfile.variables:
        zeta  = ncfile.variables['sossheig'][nemo_kt[i1],:,:]
    else:
        zeta = 0
        print('No "sossheig" var in NEMO file; substituting zero.')
    ncfile.close()
    if w2 != 0: # if no exact time match get weighted average
        print('Interpolating between times:\n {}\n{}'.format(tsnap[i1],tsnap[i2]))
        ncfile = nc.Dataset(nemo_file[i2])
        nemo_wtmp2 = ncfile.variables[t_name][nemo_kt[i2],:,:,:]
        nemo_sals2 = ncfile.variables[s_name][nemo_kt[i2],:,:,:]
        if 'sossheig' in ncfile.variables:
            zeta2  = ncfile.variables['sossheig'][nemo_kt[i2],:,:]
        else:
            zeta2 = 0
        ncfile.close()
        nemo_wtmp = nemo_wtmp*w1 + nemo_wtmp2*w2
        nemo_sals = nemo_sals*w1 + nemo_sals2*w2
        zeta = zeta*w1 + zeta2*w2
    
    return nemo_wtmp,nemo_sals,zeta


def CtSr2TS(ct,sr, gdept, h, zeta, lon, lat):
    """Convert conservative temperature, reference salinity to in-situ temperature, practical salinity
    Inputs:
        ct,sr   (layer,m,...)   conservative temperature, reference salinity;
                                1st dimension is layers, subsequent dimensions are
                                nodes, e.g. (layer,node) for fvcom or (layer,y,x) for nemo
        gdept   (layer,m,...)   depths of layers at zero sea level elevation; 
                                positive up, m, i.e. depths are negative below MSL
        h       (m,...)         bathymetry at nodes
        zeta    (m,...)         ssh
        lon,lat (m,...)         node coordinates
    Outputs:
        t,s     (layer,m,...)   in-situ temperature, practical salinity
    """
    
    # hack: ensure non-zero depth to avoid division by zero
    if type(h) is not np.ndarray:
        h = np.array([h])
    h[h==0] = 1
    
    z = gdept*(h+zeta)/h # layer depths stretched according to zeta(t)
    
    if np.sum(z>0) > np.sum(z<0):
        warnings.warn('CtSr2TS: Depths supplied to gsw.p_from_z seem to be positive down; needed positive up.', Warning)
    if np.any(z>5):
        warnings.warn('CtSr2TS: Depths >+5m supplied; This will result in T=nan for these values.', Warning)
    
    # sea pressure from height
    p = gsw.p_from_z(z, lat)
    # z : array-like;    Depth, positive up, m
    # lat : array-like;  Latitude, -90 to 90 degrees
    # p : array-like, dbar; sea pressure ( i.e. absolute pressure - 10.1325 dbar )
    
    # Calculates Practical Salinity (PSS-78) from Reference Salinity, g/kg
    s = gsw.SP_from_SR(sr)
    
    # Absolute Salinity from Practical Salinity
    sa = gsw.SA_from_SP(s, p, lon, lat)
    # SP : array-like;  Practical Salinity (PSS-78), unitless
    # p : array-like;   Sea pressure (absolute pressure minus 10.1325 dbar), dbar
    # lon : array-like; Longitude, -360 to 360 degrees
    # lat : array-like; Latitude, -90 to 90 degrees
    
    # in-situ temperature from the Conservative Temperature of seawater
    t = gsw.t_from_CT(sa, ct, p)
    # SA : array-like; Absolute Salinity, g/kg
    # CT : array-like; Conservative Temperature (ITS-90), degrees C
    # p : array-like;  Sea pressure (absolute pressure minus 10.1325 dbar), dbar
        
    return t,s


def its_from_two_sources(x,y, z1,t1,s1, z2,t2,s2, xypoly,xr,yr,rwidth):
    """Initial TS from two fields at same nodes but at different depth levels.
    
    """
    
    # interpolate from z2 to z1
    i1,i2,w1,w2 = interp.interpolant1d(z1, z2)
    s2 = s2[:,i1]*w1[:,0] + s2[:,i2]*w2[:,0]
    t2 = t2[:,i1]*w1[:,0] + t2[:,i2]*w2[:,0]
    
    # merge
    t = merge_two_fields(x,y,t1,t2,xypoly,xr,yr,rwidth)
    s = merge_two_fields(x,y,s1,s2,xypoly,xr,yr,rwidth)
    
    return t,s


def merge_two_fields(x,y,s1,s2,xypoly,xr,yr,rwidth):
    """Merge two 3D fields defined on same nodes.
    
    Merge within the buffer zone defined by a refence polyline and a polygon,
    with weights linearly changing based on the distance from the reference polyline.
    For correct merging, the reference polyline should be a subset of the polygon.
    
    Can be used to merge TS fields interpolated from NEMO and custom climatology.
    
    Second dimension for s1,s2 can represent zsl or siglay 
    as long as it is the same for both s1 and s2.
    
    Inputs:
        x,y     (node,)         fvcom grid node coordinates
        s1,s2   (node,siglay)   fields to merge
        xypoly  (pnode,2)       x,y coordinates of the polygon where the values
                                from s2 should be used
                                (can contain several nan-separated loops)
        xr,yr   (rnode,)        reference polyline to calculate merging weights
                                (can be nan-separated);
                                the weights are calculated only for the nodes 
                                within xypoly;
                                all nodes outside xypoly get values from s1;
                                
    Output:
        sm      (node,siglay)   merged field
    """
    
    s2weight = weight_from_distance_to_polyline(x,y, xr,yr, rwidth,linear=True)
    inp = mgeom.inpoly(x,y,xypoly)
    not_inp = [not k for k in inp]
    s2weight[not_inp] = 0
    s2weight = s2weight[:,None] # add dimension so it can be broadcast to s1,s2
    sm = s1*(1-s2weight) + s2*s2weight
    return sm


def its_from_tri_climatology(x,y,tri,cx,cy,ctri,ct,cs,ctime,time_start):
    """Initial TS interpolated from climatology fields on a triangular grid.
    
    Inputs:
        x,y                (node,)  fvcom grid nodes
        tri               (nele,3)  fvcom grid triangulation array, zero-based
        cx,cy,ctri                  climatology grid
        ct,cs       (cnodes,ctime)  T,S climatology values
        time_start             str  fvcom start time in '%Y-%m-%d %H:%M:%S' format
        
    Outputs:
        t,s     T,S fields interpolated to fvcom nodes (no interpolation to sigma-layers)
    """

    # expand climatology by appending the 1st value to the end so it spans full year (366 days)
    ctime = np.r_[ctime, ctime[0]+366] # unwrap to the next year
    cs = np.concatenate((cs, cs[:,:,[0]]), axis=2) # 3rd dimension is time
    ct = np.concatenate((ct, ct[:,:,[0]]), axis=2)
    
    tstart = datetime.datetime.strptime(time_start,'%Y-%m-%d %H:%M:%S')
    # climatology time should be in days since 0001-1-1 in datetime format, 
    # and since 0000-1-1 in matlab datenum format;
    # move model time to the same year as climatology
    tstart = date2num(tstart.replace(year=1))
    # ensure tstart is within the climatology time frame
    if tstart<ctime[0]:
        tstart = tstart + 366
    
    # interpolate to tstart 
    i1,i2,w1,w2 = interp.interpolant1d(tstart, ctime)
    st = cs[:,:,i1]*w1 + cs[:,:,i2]*w2
    tt = ct[:,:,i1]*w1 + ct[:,:,i2]*w2
    
    # interpolate to fvcom nodes
    nid,w,outside = mesh.tinterpolant(cx,cy,ctri,x,y)
#    s = (w[:,0]*st[nid[:,0],:] + 
#         w[:,1]*st[nid[:,1],:] + 
#         w[:,2]*st[nid[:,2],:])
#    s = np.einsum('ij,ijk->ik',w,st[nid,:]) # same, but more efficiently
    s = np.einsum('ij,ij...->i...',w,st[nid,:]) # same, but for arbitrary number of dimensions
    t = np.einsum('ij,ij...->i...',w,tt[nid,:])
    
    s[outside,:] = np.nan # nans are same for all layers, same for t and s
    t[outside,:] = np.nan
    
    # Fill in the missing values and smooth the filled part (see ts_intp_fill_and_smooth.m)
    s = mesh.fill_laplacian(tri,s) # values to (nodes,layers) shape
    t = mesh.fill_laplacian(tri,t)
    s = mesh.smooth_laplacian(tri,s,ifix=~outside)
    t = mesh.smooth_laplacian(tri,t,ifix=~outside)
    
    return t,s


def its_from_jakes_climatology(time_start,x,y,tri,utmzone,jakesfile):
    """Interpolate T,S from Jake Galbright's climatology for NE Pacific to the specified nodes.
    """
    
    ctime,cz,cs,ct,clon,clat,ctri,ch = get_jakes_climatology(jakesfile)
    
    utmproj = Proj("+proj=utm +zone="+str(utmzone))
    cx,cy = utmproj(clon,clat)
    
    t,s = its_from_tri_climatology(x,y,tri,cx,cy,ctri,ct,cs,ctime,time_start)
    
    # arange z surface to bottom
    cz = np.flip(-cz,axis=0) # and make positive
    s = np.flip(s,axis=1) # 2nd axis is depth
    t = np.flip(t,axis=1)
    
    return cz,t,s


def get_jakes_climatology(mat_file_name):
    """Read Jake Galbright's climatology for NE Pacific
    from a mat file with variables obtained with Max/Tools/IO/read_jake.m
    """
#    mat_file_name = '/media/krassovskim/MyPassport/Data/OSD/climatologies/nep35_ts.mat'
    c = sio.loadmat(mat_file_name)
    
    # time is in days since 0001-1-1 in python datetime format,
    # and since 0000-1-1 in matlab datenum format
    time = c['time'].astype(float)[:,0] # make it float and 1d
    z = c['z'].astype(float)[:,0] # vertical levels
    s = c['s'] # salinity, 3d (nodes,depth,time)
    t = c['t'] # temperature
    
    # triangular grid
    lon = c['lon'][:,0] # make it 1d
    lat = c['lat'][:,0]
    tri = c['tri'] - 1 # make zero-based
    h = c['h'][:,0] # bathymetry on the grid
    
    return time,z,s,t,lon,lat,tri,h

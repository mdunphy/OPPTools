#import sys
# sys.path.insert(0, 'c:/pramod/work-codes/pytools/lib/')
import numpy as np
from netCDF4 import Dataset as ncdata 
#from fvcomToolbox import addTimeVariables
# from scipy.interpolate import interp2d 
#import math
import glob

#~ ============================================================================
def list_nemo_output(vname, input_dir):
    '''
    List NEMO NEP036 output files of one type in the specified folder.
    File naming convention is that adopted for NEMO files trimmed to cover the FVCOM domain, e.g.
    NEP036-N30_IN_20150301_00241920_grid_T_FVCOM.nc
    The file type is specified by vname, which can be one of the following:
    T,U,W
    '''
    
    searchstr = '{:s}/*_grid_'+vname+'_FVCOM.nc'
    flist = glob.glob( searchstr.format(input_dir) )
    flist = sorted(flist)
    if len(flist)==0:
        raise Exception('No files found in the specified folder.')
    
    return flist

#~ ============================================================================
def nemo_ref_time(fname):
    '''
    Extract reference time for NEMO time series
    '''
     
    ncfile = ncdata(fname)
    tvar = ncfile.variables['time_counter']
    date0 = tvar.units # must contain something like: 'seconds since 2014-9-15 00:00:00'
    date0 = date0.replace("seconds since ", "") # keep only '2014-9-15 00:00:00'
    
    return date0
    
#~ ========================================================================================    
def read_nemo_grid(fname):
    ncfile = ncdata(fname, 'r')
    lon = ncfile.variables['nav_lon'][:]
    lat = ncfile.variables['nav_lat'][:]
    dep = ncfile.variables['Bathymetry'][:]
    ncfile.close() 
    return lon, lat, dep
#~ ========================================================================================    
def read_grd(fname):
    fid = open(fname, 'r')
    l = fid.readlines()
    fid.close()
    nnode = int(l[0].split('=')[1])
    nelem = int(l[1].split('=')[1])
    nv    = np.asarray([ll.split()[1:4] for ll in l[2:2+nelem]], dtype = np.int)
    ndxy  = np.asarray([ll.split()[1:3] for ll in l[2+nelem:2+nelem+nnode]], dtype = np.float32)
    
    return nv, ndxy
#~ ========================================================================================    
def read_dep( fname ):
    fid = open(fname, 'r')
    l = fid.readlines() 
    fid.close()
    dep = np.asarray([ ll.split()[2] for ll in l[1:] ]).astype(float)
    return dep
##~ ========================================================================================
#def make_type3_nesting_file():
#
#    pass
    
#~ ========================================================================================
def identify_nesting_zone( obc_nodes, rad, ndxy, nv ):
    nnodes = np.shape( ndxy )[0]
    print(nnodes)
    nlist = []; dslist = []
    for n in np.arange(nnodes):
        xy = ndxy[n,:]
        ds = np.sqrt( (ndxy[obc_nodes-1, 0] - xy[0])**2 + (ndxy[obc_nodes-1, 1] - xy[1])**2 )
        a = np.argmin(ds)
        if ds[a] < rad:
            nlist.append(n + 1)
            dslist.append(ds[a])
    return nlist, dslist
#~ =========================================================================================
def interpolate_NEMO_bathymetry( x_nemo, y_nemo, v_nemo, x_intp, y_intp ):
    xynemo = np.stack((x_nemo, y_nemo), axis = 1)
    xyintp = np.stack((x_intp, y_intp), axis = 1)
    nlist, ndist = idwSetup(xynemo, xyintp, 5000.00)
    #~ print nlist, ndist 
    v_intp = idwIntp(nlist, ndist, v_nemo, 2.0)
    print(v_intp)
    return v_intp 
#~ =========================================================================================
def radClosest(xyintp, xy, rad):
    ds  = np.sqrt( (xy[:,0] - xyintp[0])**2  + (xy[:,1] - xyintp[1])**2 )
#    idx = np.arange(len(ds))
    i = np.where (ds < rad)
    L = np.asarray(i).flatten() 
    dist = np.asarray(ds[i]).flatten() 
    
    return L, dist
#----------------------------------------------------------------------------
# setup IDW interpolation matrix.
#   find the npts closest points and save the matrix.
#----------------------------------------------------------------------------
def idwSetup(xy, xyintp, rad): 
    nList = []
    nDist = []
    for n in range( np.shape(xyintp)[0] ):
        l, dist = radClosest( xyintp[n,:], xy, rad )
        nList.append(l)
        nDist.append(dist)
        
    return nList, nDist
#----------------------------------------------------------------------------
# do the interpolation. 
#----------------------------------------------------------------------------
def idwIntp(nList, nDist, obsVal, p, excludeZeros = True):
    intpVal = []
    for k in range(len(nDist)): # for each interpolation point
#        dist  = nDist[k]
#        nghn  = nList[k]
#        dist_list = [] 
#        nghn_list  = [] 
#        #~ remove values that are NaN's or such from the interpolation process!
#        for i in range( len(nghn) ): # for each node in the list of closest nodes
#            if math.isnan( obsVal[ nghn[i] ] ) or obsVal[nghn[i]] == 0.:
#                #~ print obsVal[nghn[i]]
#                pass
#            else:
#                dist_list.append( dist[i] )
#                nghn_list.append( nghn[i] )
#                
#        dist  = np.asarray( dist_list )
#        nghn  = np.asarray( nghn_list )
#
#        nVar  = [ obsVal[j] for j in nghn ]
#        wt    = [ 1./(dist[j]**p) for j in range(len(dist)) ]
#        wtSum = np.sum( wt )
#        tmp = np.dot( wt, nVar )
#        intpVal.append( tmp/wtSum ) 
        
        dist  = np.asarray(nDist[k]).copy()
        nghn  = np.asarray(nList[k]).copy()
        nVar = obsVal[nghn]
        ival = ~np.isnan(nVar) # exclude NaNs from interpolation
        if excludeZeros:
            ival = np.logical_and(ival,nVar!=0.) # exclude zeros from interpolation if needed
            
        wt = 1./(dist[ival]**p)
        intpVal.append( np.dot(wt,nVar[ival])/np.sum(wt) )
    return np.asarray(intpVal)

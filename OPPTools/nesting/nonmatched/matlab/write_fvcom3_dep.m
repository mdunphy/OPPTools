function write_fvcom3_dep(casename,x,y,z)

% ensure x,y,z are row vectors
x = x(:)';
y = y(:)';
z = z(:)';

nn = length(x);
fout = fopen([casename '_dep.dat'],'w');
fprintf(fout,'Node Number = %d\n',nn);
fprintf(fout,'%#15.7f %#15.7f %#15.7f\n',[x; y; z]);
fclose(fout);
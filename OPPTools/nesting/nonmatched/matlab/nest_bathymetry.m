function [zm,kr,kt] = nest_bathymetry(x,y,z,xo,yo,xn,yn,zn,r,rt)
% Merge bathymetry in the relaxation zone of fvcom grid with nemo bathymetry.
% x,y,z     fvcom bathymetry z at nodes x,y
% xo,yo     open boundary polyline (NaN-separated in case of several OB segments)
% xn,yn,zn  nemo bathymetry zn at nodes xn,yn
% r         relaxation zone radius: distance from fvcom open boundary
% rt        transition zone radius: distance from fvcom open boundary; rt>r

% addpathrel('Tools\Geodesy')
% [xn,yn] = geo2utm(lat,lon,utmzone);
% To get open boundary polyline: generate it in kit4_openboundary.vg
% lob       logical index for open boundary nodes; lob = code==5 | code==6;

% IDs (linear indices) for fvcom nodes within relaxation+transition zone
[k,d] = range_zone(x,y,xo,yo,rt);
xk = x(k);
yk = y(k);

% separate relaxation and transition zone
kkr = d<=r; % within relaxation zone range
kr = k(kkr); % relaxation zone
kt = k(~kkr); % transition zone
% weights for transition zone, ranging from 0 at the outer boudary of the
% transition zone to 1 at the inner boundary
w = (d(~kkr) - r)/(rt-r);

% interpolate nemo bathymetry to fvcom within relaxation+transition zone
tri = delaunay(xn,yn);
i = tsearch(xn,yn,tri,xk,yk); %#ok<DGEO4> Tools/Geometry/Triangulation
zi = tinterp([xn yn],tri,zn,[xk yk],i); % triangle-based linear interp
% Performs nearest-neighbour extrapolation for points outside the
% triangulation.

zm = z; % initialize output

zm(kr) = zi(kkr); % relaxation zone depths

% transition from nemo bathymetry to fvcom bathymetry within transition zone
% with linear weighting based on the distance from OB
zm(kt) = zi(~kkr).*(1-w) + z(kt).*w;

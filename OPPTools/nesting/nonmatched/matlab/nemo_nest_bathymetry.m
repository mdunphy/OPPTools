function nemo_nest_bathymetry()

addpathrel('Tools\Geodesy')
addpathrel('Tools\Modelling');

% nemo grid
fnemo = '/media/user/My Passport/Data/fvcom/nemo/Bathymetry_EastCoast_NEMO_R036_GEBCO_corrected_lat6_v12.nc';
nlon = double(ncread(fnemo,'nav_lon'));
nlat = double(ncread(fnemo,'nav_lat'));
zn = double(ncread(fnemo,'Bathymetry'));
nlon = nlon(:);
nlat = nlat(:);
zn = zn(:);
[xn,yn] = geo2utm(nlat,nlon,9);

% fvcom grid (with original depths)
fkit = '/media/user/My Passport/Data/fvcom/nemo/nemo_fvcom_system/kit4/input/kit4_lonlat_grd.dat';
[glon,glat,tri,z] = read_fvcom3_grd(fkit);
[x,y] = geo2utm(glat,glon,9);

% open boundary polyline
ob = load('obc159'); % exported from Projects/Femgrids/nemo/kit4_openboundary.vg


% bathymetry adjustment for relaxation+transition zone
r = 50000; % 50 km relaxation zone
tic
% 50 km relaxation zone + 25 km transition zone:
[zm,kr,kt] = nest_bathymetry(x,y,z,ob.r.x',ob.r.y',xn,yn,zn,r,r*1.5);
toc
% save('nest_bathymetry_50km','zm','kr','kt')


% write dep file with modified depths
fdep_orig = '/media/user/My Passport/Data/fvcom/nemo/nemo_fvcom_system/kit4/input/kit4_dep.dat';
[x,y] = read_fvcom3_dep(fdep_orig);
write_fvcom3_dep('kit4_nemo',x,y,zm)
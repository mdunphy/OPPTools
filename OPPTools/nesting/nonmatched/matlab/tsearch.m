function k = tsearch(x,y,tri,xi,yi)
%TSEARCH Replacement for Matlab's native tsearch function.
%   Calls compiled mex file depending on the platform.

% M.Krassovski 2015-07-06

if isunix
    % Call 
    % https://people.sc.fsu.edu/~jburkardt/m_src/tsearch/tsearch.html
    
    % This function is about as fast as MATLAB's tsearch, but does 
    % not have the restriction that the triangulation be a Delaunay 
    % triangulation.
    % 
    % Type "mex mytsearch" in Matlab to compile.
    % by David R. Martin, Boston College
 
    k = tsearch_mex(x,y,tri,xi,yi);
else
%     %TODO %%%% Compile tsearch_mex file for Windows.
%     error('Compile tsearch_mex file for Windows.')
    
%     addpathrel('Projects\Femgrids\Tools')
%     k = tsearchsafe_mk(x, y, tri, xi, yi);

    % Solution from http://www.mathworks.com/matlabcentral/fileexchange/25555-mesh2d-automatic-mesh-generation
    % "Since Mathworks abandoned tsearch, simply calling tsearchn instead
    % works for me."
    k = tsearchn([x(:) y(:)],tri,[xi(:) yi(:)]); % for Delaunay only?
end
function write_ncmask(mask,varname,casename)
% Create nc file with land mask.
% mask  should be in [x,y[,z]] shape, i.e. 2D or 3D.

sz = size(mask);
nd = length(sz);
if nd==2
    masktype = '2D';
elseif nd==3
    masktype = '3D';
else
    error('mask should be 2D or 3D array.')
end

fname = [casename '_' varname '_mask.nc'];

% CLOBBER Overwrite any existing file with the same name.
ncid = netcdf.create(fname, 'CLOBBER'); % format='NETCDF3_CLASSIC'

netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'title', 'Land mask')
netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'convention', '0 - land, 1 - water')
netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'history', 'created using nemo_mask.m')

% Define dimensions in the file.
dimx = netcdf.defDim(ncid,'x', sz(1));
dimy = netcdf.defDim(ncid,'y', sz(2));
dims = [dimx dimy];
if nd==3
    dimz = netcdf.defDim(ncid,'z', sz(3));
    dims = [dims dimz];
end

% Define variables in the file.
varmask = netcdf.defVar(ncid,'mask',netcdf.getConstant('NC_INT'),dims);
netcdf.putAtt(ncid,varmask,'long_name','Land mask')
netcdf.putAtt(ncid,varmask,'grid', varname)
netcdf.putAtt(ncid,varmask,'type', masktype)

% Leave define mode and enter data mode to write data.
netcdf.endDef(ncid);

% Write data to variable.
netcdf.putVar(ncid,varmask,int32(mask));

netcdf.close(ncid)
function [x,y,tri,z] = read_fvcom3_grd(fname)
% Read casename_grd.dat file from FVCOM input.

g = importdata(fname,' ',2); % 2 header lines

% addition for fvcom3.2(?): depths recorded in the 4th column of nodes array
% hack importdata output
% w = strsplit(g.textdata{1,1}); % split in words
% nn = str2double(w{4}); % number of nodes
ne = str2double(g.textdata{2,4}); % number of elements

g = g.data; % 4-column with NaNs in the 4th column for nodes array

%%% not working if depths recorded in the 4th column of nodes array
% ln = isnan(g(:,4)); % rows for nodes array
% tri = g(~ln,2:4);
% x = g(ln,2);
% y = g(ln,3);

tri = g(1:ne,2:4);
x = g(ne+1:end,2);
y = g(ne+1:end,3);
z = g(ne+1:end,4);
function av = nemo_fvcom_azimuth()
% Local angle between nemo NEP036 (fvcom part, 290x300) grid lines and local meridian
% in degrees CW from north.
% Uses azimuth from mapping toolbox.

% take T-file: T-grid is the same as model grid
fnemo = '/media/user/My Passport/Data/fvcom/nemo/NEP_FVCOM_backup/NEP036-N07_IN_20130915_00000720_grid_T_FVCOM.nc';

[~,av] = nemo_azimuth(fnemo);

av = [av av(:,end)]; % duplicate last j-index to match the size of U,V in data files

% write an ascii file for further use in fvcom-NEMO-interp_UV.py
dlmwrite('nemo_fvcom_azimuth.txt',av)
function files = listfiles(indir,ext,sub_flag)
% List files in the specified directory indir.
% ext   [optional] list only files with the specified extension.
%       for ext==[], list files with any extension (including files without extension);
%       for ext=='', list only files without extension.
% sub_flag  false (default): exclude subdirectories;
%           true:            include subdirectories all the way deep.
% Output is a struct array. Get names as files(i).name.

if ~exist('indir','var') || isempty(indir); indir = '.'; end
% remove trailing separators
while indir(end) == filesep
    indir(end) = [];
end
if ~exist('ext','var')
    ext = []; % default: all extensions (including files without extension)
end
if ~exist('sub_flag','var'); sub_flag = false; end

% get directory content
if isempty(ext)
    files = dir(indir);  % list files in main folder (including files without extension)
    if ischar(ext) % empty string, ext=='';
        % exclude files with extensions
        files(~cellfun(@isempty,strfind({files.name},'.'))) = [];
    end
else
    files = dir([indir filesep '*.' ext]);  % list files in main folder with particular extension
end

% add path to names
if ~strcmp(indir,'.')
    for ifile = 1:length(files)
        files(ifile).name = [indir filesep files(ifile).name];
    end
end

if sub_flag     % search subdirectories
    dirs = mkListDirs(indir);       % list subfolders
    for idir = 1:length(dirs)
        subfiles = mkListFilesDeep([indir filesep dirs(idir).name], ext);
        files = [files; subfiles];  % append files in current subfolder to the list
    end
end

% remove directories
files([files.isdir]) = [];


function files = mkListDirs(dirName)
% returns the list of all subdirectories in a specified directory
% as struct array. Get names as files(i).name

files = dir(dirName);

files(~[files.isdir]) = [];   % remove non-directories

% Remove two psedofiles (. and ..) from the file list
files(strcmp({files.name},'.')) = [];
files(strcmp({files.name},'..')) = [];


function files = mkListFilesDeep(indir,ext)
% Returns the list of all files with .ext in all subdirectories of
% specified directory 'indir' all way deep.
% Output is a struct array. Get names as files(i).name

% files = dir([indir filesep '*.' ext]);  % list files in main filder
% get directory content
if isempty(ext)
    files = dir(indir);  % list files in main folder (including files without extension)
    if ischar(ext) % empty string, ext=='';
        % exclude files with extensions
        files(~cellfun(@isempty,strfind({files.name},'.'))) = [];
    end
else
    files = dir([indir filesep '*.' ext]);  % list files in main folder with particular extension
end

% add path to names
for ifile = 1:length(files)
    if ~strcmp(indir,'.')
        files(ifile).name = [indir filesep files(ifile).name];
    end
end

dirs = mkListDirs(indir);       % list subfolders
for idir = 1:length(dirs)
    subfiles = mkListFilesDeep([indir filesep dirs(idir).name], ext);
    files = [files; subfiles];  % append files in current subfolder to the list
end

% remove directories
files([files.isdir]) = [];
# Control parameters for generation of FVCOM-NEMO nesting file.

NEMO_PATH = '/media/krassovskim/MyPassport/Data/fvcom/nemo/'
KIT_PATH  = NEMO_PATH+'fvcom-NEMO-nesting/'
#NEMO_PATH = 'D:\\Data\\fvcom\\nemo\\'
#KIT_PATH  = NEMO_PATH+'fvcom-NEMO-nesting\\'

#======================================
# For both fvcom-NEMO-interp_UV.py and generate_type3_nesting_file.py

# NEMO output (trimmed NEP036)
input_dir     = NEMO_PATH+'NEMO_NEP036_2015' # 2015 test case
#input_dir     = NEMO_PATH+'NEMO_NEP036_2015md' # 2015 test case

# Output from fvcom-NEMO-interp_UV.py:
output_prefix = KIT_PATH+'nesting_zone_interp_2015/NEP036_intp' # 2015 test case
#output_prefix = KIT_PATH+'nesting_zone_interp_2015md\\NEP036_intp' # 2015 test case

nznodes_file  = KIT_PATH+'nesting-zone-50km-nodes.txt' # fvcom grid nodes
nzcentr_file  = KIT_PATH+'nesting-zone-50km-centr.txt' # fvcom element centroids

#======================================
# For fvcom-NEMO-interp_UV.py (interpolation to fvcom nesting zone)

# NEMO grid orientation (generated by nemo_fvcom_azimuth.m)
nemo_azimuth_file = KIT_PATH+'nemo_fvcom_azimuth.txt'

# Land masks for the trimmed NEP036 (generated by nemo_mask.m)
mask_n_file = KIT_PATH+'nemo_fvcom_node3d_mask.nc'
mask_u_file = KIT_PATH+'nemo_fvcom_u_mask.nc'
mask_v_file = KIT_PATH+'nemo_fvcom_v_mask.nc'

#======================================
# For generate_type3_nesting_file.py

# NEMO reference time
# In case date0 is left empty, date0 will be determined automatically from the NEMO output:
#time_counter
#           Attributes:
#                       units         = 'seconds since 2014-9-15 00:00:00'
date0 = '' # determine from the NEMO output
# Can be set to a specific time in case it is not recorded correctly in the NEMO output.
#date0 = '2013-09-15 00:00:00' # NEMO start time: 2013 test case
#date0 = '2014-09-15 00:00:00' # NEMO start time: 2015 test case

# Standard FVCOM grid files
fvcom_grd_file   = KIT_PATH+'kit4_lonlat_grd.dat'
fvcom_dep_file   = KIT_PATH+'kit4_nemo_dep.dat'
fvcom_sigma_file = KIT_PATH+'kit4_sigma.dat'

# Forcing ramp-up (number of time steps from the start of the NEMO series)
nrampup = 24*3 # 3-day ramp-up for hourly data

# Option to generate barotropic or baroclinic forcing
#opt              = 'BRTP'
opt              = 'BRCL'

# Name for the forcing file to generate:
#output_file      = KIT_PATH+'test_2015md_'+opt.lower()+'.nc' # test_2015md_brpt.nc
output_file      = KIT_PATH+'test_2015md1m_'+opt.lower()+'.nc' # test_2015md1m_brpt.nc
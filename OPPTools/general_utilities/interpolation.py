"""
1d and 2d interpolation routines.
"""
import numpy as np
import scipy.spatial
import matplotlib.tri as mtri


def interpolant1d(zi,z,zmask=None,extrapolate=True,extrap_threshold=None):
    """ 1d linear interpolant.
    
    Works on n-d arrays along 1st dimension.
    
    Parameters
    ----------
    
        zi : array_like of shape (n,d1,...)
            N-d array or scalar. Coordinate of `n` points to interpolate to.
        z : array_like of shape (m,d1,...)
            Points to interpolate from. Second and subsequent dimensions must 
            match those of `zi`.
        zmask : array_like of shape (m,d1,...), optional
            Mask for `z` with 0's for invalid points.
        extrapolate : bool
            If True, do nearest neighbour extrapolation,
            if False, produce nans for out-of-range `zi` values.
        extrap_threshold : float, optioanl
            Extrapolate no further than the threshold (same units as `z` and `zi`).
            No threshold by default.
        
    Returns
    -------
    
        i1,i2 : array_like of shape (n,d1,...)
            Interpolation indices, ravelled to use with (m,d1,...) arrays.
        w1,w2 : array_like of shape (n,d1,...)
            Corresponding weights.
    
    Example
    -------
    
    To apply interpolant:
        vi = np.take(v,i1)*w1 + np.take(v,i2)*w2 # where v has z.shape
    """
    # generalize for inputs of various shapes, including scalars
    scalar = np.isscalar(zi)
    if not scalar:
        sz = zi.shape
    zi,z = atleast_2d0(zi,z)
    
    if zmask is not None:
        z = np.ma.masked_array(z,mask=zmask==0) # apply
    
    n = zi.shape[0]
    i1,i2 = [np.zeros(zi.shape, dtype=int) for i in range(2)] # initialize
    d1,d2 = [np.zeros(zi.shape)            for i in range(2)] # initialize
    
    for kz in range(n):
        dz = z - zi[kz,:]
        dza = np.ma.masked_array(dz.copy(), mask=dz>0) # z smaller than zi
        dzb = np.ma.masked_array(dz.copy(), mask=dz<0) # z greater than zi
        # indices; zeros for out of range zi
        i1[kz,:] = dza.argmax(axis=0)
        i2[kz,:] = dzb.argmin(axis=0)
        # distances
        # masked for zi below the range, nearest neighbour for zi above the range:
        da = np.abs(dza.max(axis=0))
        # masked for zi above the range, nearest neighbour for zi below the range:
        db =        dzb.min(axis=0)
        d1[kz,:] = da.filled(0) # weights; substitute zeros for out of range zi
        d2[kz,:] = db.filled(0)
    
    # exact matches between z and zi
    hits = np.logical_and(d1==0,d2==0) # also includes completely masked z along 1st dimension
    # out of range
    extrapa = np.logical_and(d1==0,d2!=0) # these points need extrapolation below
    extrapb = np.logical_and(d1!=0,d2==0) # these points need extrapolation above
    
    if extrap_threshold is not None:
        extrapa = np.logical_and(extrapa,d2<=extrap_threshold)
        extrapb = np.logical_and(extrapb,d1<=extrap_threshold)
    
    # weights: inverse distances; zeros for out of range zi, inf for exact matches
    with np.errstate(divide='ignore'):
        w1 = 1/d1
        w2 = 1/d2
    
    # substitute weights for exact matches
    w1[hits] = 1
    w2[hits] = 0
    # out of range
    w1[extrapa] = 0 # w2[extrapa] is non-zero
    w2[extrapb] = 0 # w1[extrapb] is non-zero
    if not extrapolate:
        w1[extrapb] = np.nan # out of range zi will get nans after interpolant is applied
        w2[extrapa] = np.nan
    
    # deal with completely masked nemo nodes
    if zmask is not None:
        allmasked = np.all(zmask==0,axis=0)
        w1[:,allmasked] = np.nan
        w2[:,allmasked] = np.nan
    
    # normalize weights
    ws = w1+w2
    with np.errstate(invalid='ignore'):
        w1 = w1/ws
        w2 = w2/ws
    
    # ravel indices for subsequent indexing of arrays
#    ic = np.ones((n,1),dtype=int) * np.arange(z.shape[1],dtype=int) # 2d z
    dim1 = zi.shape[1:]    # 2nd and subsequent dimensions
    dim1prod = np.prod(dim1) # number of nodes
    # indices of all columns (ravelled for 2nd and subsequent dimensions) tiled n times
    ic = np.repeat(np.arange(dim1prod,dtype=int).reshape(dim1)[None,:],n,axis=0)
    i1 = np.ravel_multi_index((i1,ic),(z.shape[0],dim1prod))
    i2 = np.ravel_multi_index((i2,ic),(z.shape[0],dim1prod))
    
    if scalar:
        i1 = np.asscalar(i1)
        i2 = np.asscalar(i2)
    else:
        # keep dimensions of the input zi
        i1 = i1.reshape(sz)
        i2 = i2.reshape(sz)
    
    return i1,i2,w1,w2


def atleast_2d0(*arys):
    """ As numpy.atleast_2d, but populates 1st dimension first.
    View inputs as arrays with at least two dimensions.
    Code is from numpy.atleast_2d
    """
    res = []
    for ary in arys:
        ary = np.asanyarray(ary)
        if ary.ndim == 0:
            result = ary.reshape(1, 1)
        elif ary.ndim == 1:
            result = ary[:,np.newaxis] #mk: the only change 
        else:
            result = ary
        res.append(result)
    if len(res) == 1:
        return res[0]
    else:
        return res


def interpolant2d(pi,p,r=None,method='triangulation',p_grid_shape=None,
                  p_grid_mask=None):
    """ Inverse distance 2d interpolant to interpolate from a regular 
    orthogonal grid to arbitrary nodes.
    
    Parameters
    ----------
    
        pi : array_like of shape (n,2)
            [x,y]  coordinates of points to interpolate to.
        p : array_like of shape (m,2)
            [x,y]  coordinates of points to interpolate from. These are 
            ravelled coordinate arrays for the regular grid. Can contain inf 
            for invalid points, e.g. for dry land nodes.
        r : array_like of shape (m,2)
            Used only for method='kdtree'. Distance for each p to use as a 
            threshold: if `p` is further than `r` from `pi` it is not included 
            in the interpolant. If a scalar is supplied, it is assumed to be 
            same for all p.
        method : str, optional
            Either 'triangulation' or 'kdtree'. 'triangulation' method is more 
            robust since, for each interpolation point, it always picks four 
            nodes from same grid cell. 'kdtree' is faster but gives 
            biased results near grid cell corners. Default is 'triangulation'.
        p_grid_shape : tuple
            Shape of the original grid, before ravelling, (Nj,Ni). Used only 
            with method='triangulation'.
        p_grid_mask : array_like of shape (m,), bool, optional
            Ravelled mask array for the regular grid. Should contain `False` for 
            masked/invalid nodes, `True` for valid nodes.
    
    Returns
    -------
    
        loc : array_like of shape (n,4)
            Indices into p.
        w : array_like of shape (n,4)
            Corresponding weights.
        
    Example
    -------
    
        To apply interpolant to a field `v`:
        v_intp = np.sum(np.take(v.ravel(),loc) * w, axis=2)
    """
    
    if method == 'kdtree':
        if np.isscalar(r):
            r = np.tile(r, p.shape[0])
        
        kn = 4 # kn nearest neighbours
        
        with np.errstate(invalid='ignore'): # inf's in p are ok
            tree = scipy.spatial.KDTree(p) # Python 3, use zip(x,y) in Python 2
        w,loc = tree.query(pi,k=kn)
        
        w[w>r[loc]] = np.inf # invalidate w for p further than threshold
        
    else: # method='triangulation'
        ii,jj = np.meshgrid(np.arange(p_grid_shape[1]),np.arange(p_grid_shape[0]))
        
#        # this approach has potential problems near concave boundaries of the p grid
#        tri = scipy.spatial.Delaunay(p)
#        s = scipy.spatial.tsearch(tri, pi) # -1 for pi outside triangulation
#        tris = tri.simplices[s]
        
        # workaround for concave grid boundaries
        #WARNING p must be obtained by ravelling 2D arrays of coordinates, no rearrangements!!!
        # mtri.Triangulation relies on that
        ir = np.ravel_multi_index((jj,ii),p_grid_shape)
        # construct triangles from ravelled indices
        tric1 = np.stack((ir[1:,:-1],ir[:-1,:-1],ir[1:,1:]),axis=2).reshape(-1,3)
        tric2 = np.stack((ir[1:,1:],ir[:-1,:-1],ir[:-1,1:]),axis=2).reshape(-1,3)
        tric = np.r_[tric1,tric2]
        tf = mtri.Triangulation(p[:,0],p[:,1],tric).get_trifinder() # this is slowest part
        s = tf(pi[:,0],pi[:,1])
        tris = tric[s]

        itri = np.take(ii.ravel(),tris)
        jtri = np.take(jj.ravel(),tris)
        
        # 4th node should belong to same grid cell;
        # to pick 4th node select the node which occurs once in each row
        def append4th(itri):
            i0 = itri[:,1] != itri[:,2]
            i1 = itri[:,2] != itri[:,0]
            i2 = itri[:,0] != itri[:,1]
            i4 = np.c_[np.logical_and(i1,i2),
                       np.logical_and(i0,i2),
                       np.logical_and(i0,i1)]
            return np.c_[itri,itri[i4]]
        
        itri = append4th(itri)
        jtri = append4th(jtri)
        
        loc = np.ravel_multi_index((jtri,itri), p_grid_shape) # linear indices
        d = p[loc,:] - pi[:,None,:] # coordinate differences
        w = np.sqrt(np.sum(d**2,axis=2)) # Euclidean distance from pi to each corner
        w[s==-1,:] = np.nan # for pi outside triangulation
        # sort rows by w in ascending order
        rowsort = np.argsort(w,axis=1)
        w = np.take_along_axis(w, rowsort, 1)
        loc = np.take_along_axis(loc, rowsort, 1)
        
    
    hits = w[:,0]==0    # for pi which match any p exactly
    with np.errstate(divide='ignore'):
        w = 1/w         # inverse distances
    w[hits,0] = 1       # for points that match exactly, 1st column gets 1's
    w[hits,1:] = 0      # the rest of the columns get 0's
    if p_grid_mask is not None:
        # zero out weights for masked nodes
        w[~np.take(p_grid_mask.ravel(),loc)] = 0 
    with np.errstate(invalid='ignore'): # nan's in w for out of range pi
        w = w/np.sum(w,axis=1,keepdims=True) # normalize weights to sum to 1
    
    return loc,w
"""
Tools for spectral analysis
"""
import numpy as np
from scipy.signal import welch
from scipy.stats import chi2
import matplotlib.pyplot as plt

def spectrum(x, fs=1.0, nperseg=None, noverlap=None, detrend='constant',  axis=-1, conf=0.95):
    """Power spectrum of signal `x` with confidence level estimate.
    
    Uses scipy.signal.welch with 
    window='hann', nfft=nperseg, return_onesided=True, scaling='density', average='mean'
    
    Parameters
    ----------
    x : array_like
        Time series of measurement values
    fs : float, optional
        Sampling frequency of the `x` time series. Defaults to 1.0.
    nperseg : int, optional
        Length of spectral window. Defaults to the power of 2 nearest to the 
        quarter of the series length.
    noverlap : int, optional
        Number of points to overlap between windows. If `None`,
        ``noverlap = nperseg / 2``. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    axis : int, optional
        Axis along which the periodogram is computed; the default is
        over the last axis (i.e. ``axis=-1``).
    conf : float, optional
        Confidence for which to calculate confidence limits, e.g. conf=0.95 
        gives 95% confidence limits. Defaults to 0.95.

    Returns
    -------
    f : ndarray
        Array of sample frequencies.
    Pxx : ndarray
        Power spectral density or power spectrum of x.
    c : array of length 2
        Upper and lower confidence limit relative to 1.
    """
    
    n = x.shape[axis] # series length
    
    if nperseg is None:
        nperseg = int(2**np.round(np.log2(n/4)))
    if noverlap is None:
        noverlap = np.floor(nperseg/2)
    
    f,Pxx = welch(x, fs=fs, nperseg=nperseg, noverlap=noverlap, detrend=detrend, axis=axis)
    
    # confidence limits
    Vw = 1.5 # coefficient for hann window
    k = np.floor((n-noverlap)/(nperseg-noverlap)) # number of windows
    dof = 2*np.round(k*9/11*Vw) # degrees of freedom
    c = dof/np.array(chi2.interval(conf,df=dof))
    
    return f,Pxx,c


def plot_spectrum(f,p,c=None,clabel=None,
                  cx=None,cy=None,
                  color=None,ccolor='k',
                  units='m',funits='cpd',
                  ax=None):
    """ Spectrum plot in log-log axes with confidence interval
    
    Parameters
    ----------
    f : array_like
        Array of sample frequencies.
    p : array_like
        Power spectral density or power spectrum of x.
    c : array of length 2, optional
        Upper and lower confidence limit relative to 1. Returned by spectrum()
        in `c`. No error bar by default.
    clabel : str, optional
        Label for the error bar, e.g. '95%'. No label by default.
    cx,cy : float, optional
        Coordinates (in data units) for the error bar. By default, cx is at 
        0.8 of x-axis span, and cy is at 3 lower confidence intervals above max 
        power value in the band spanning the error bar with label.
    color : matplotlib compatible color, optional
        Color cpecification for the plot line. Defaults to Matplotlib default 
        color.
    ccolor : matplotlib compatible color or 'same', optional
        Color cpecification for the error bar. If 'same', uses same color as 
        the plot line. Defaults to black, 'k'.
    units : str, optional
        Units for analysed series, e.g. 'm' for sea level, 'm/s' for velocity.
        Defaults to 'm'.
    funits : str, optional
        Frequency units. Defaults to 'cpd'.
    ax : Matplotlib axes handle, optional
        Defaults to the current axes.
    """
    
    # exclude zero-frequency value
    ival = f!=0
    f = f[ival]
    p = p[ival]
    
    if ax is None:
        ax = plt.gca()
    
    # spectrum
    hp = ax.loglog(f,p)
    ax.set_xlabel('Frequency ('+funits+')')
    ax.set_ylabel(r'PSD ('+units+')$^2$/'+funits)
#    ax.grid(True,which='both')
    ax.grid(True,which='major',linewidth=1)
    ax.grid(True,which='minor',linewidth=.5)
    
    # error bar
    if c is not None:
        # attempt of automatic error bar placement
        if cx is None:
            xlim = ax.get_xlim()
            xliml = np.log10(xlim)
            cx = 10**(xliml[0]+np.diff(xliml)*.8) # at 0.8 of x-axis span
        if cy is None:
            xlim = ax.get_xlim()
            xliml = np.log10(xlim)
            # approx x-range for error bar
            xr = 10**(np.log10(cx) + np.diff(xliml)*[-0.05,0.15])
            inx = np.logical_and(f>xr[0],f<xr[1])
            # 3 lower intervals above max power value in the band
            cy = p[inx].max() * (1/c[1])**3
        if ccolor == 'same':
            ccolor = hp[0].get_color()
        # Need an error bar that goes from cy*c[1] to cy*c[0];
        # plt.errorbar plots bar from cy-clower to cy+cupper;
        # convert c to yerr accordingly:
        yerr = cy*((c[::-1]-1)*[-1,1])[:,None]
        ax.errorbar(cx,cy,yerr=yerr,color=ccolor,capsize=5,marker='o',markerfacecolor='none')
        # capsize is the width of horizontal ticks at upper and lower bounds in points
        if clabel is not None:
            ax.text(cx,cy,'   '+clabel,verticalalignment='center',color=ccolor)


def mark_frequency(fmark,fname='',ax=None,f=None,p=None):
    """ Annotation arrow with text label above spectra plot at a specified frequency
    
    Parameters
    ----------
    
    fmark : float
        Frequency to mark
    fname : str, optional
        Text for annotation. Defaults to empty string.
    ax : axes handle, optional
        Use all line objects in these axes to determine vertical position of 
        the annotation. If supplied, f,p are ignored. Also denotes axes for 
        plotting. Defaults to None.
    f,p : array_like, optional
        x- and y-coordinates for the curve to use for vertical positioning of 
        the annotation. Default to None.
    """
    if ax is not None: # determine y-position from all lines in the axes
        lines = ax.lines
        # find highest line at this freq
        pmark = np.nan
        for l in lines:
            pmark = np.fmax(pmark,
                            np.interp(fmark, l.get_xdata(), l.get_ydata(),
                                            left=np.nan, right=np.nan))
        if np.isnan(pmark): # no lines at this frequency
            # place it in the middle
            ylim = ax.get_ylim()
            if ax.get_yscale() == 'log':
                pmark = 10**(np.mean(np.log10(ylim)))
            else:
                pmark = np.mean(ylim)
        
    elif f is not None and p is not None: # determine y-position from supplied f,p
        pmark = np.interp(fmark,f,p)
        
    else:
        raise RuntimeError('Either axes of f,p values are needed to determine y-position for frequency mark.')
    
    arrowprops = dict(arrowstyle="simple, head_width=0.25, tail_width=0.05",
                      color='k',lw=0.5,shrinkB=10)
    # shrinkB is offset in pixels from (f,p) line
    
    if ax is None:
        ax = plt.gca()
    
    ax.annotate(fname, xy=(fmark,pmark), xytext=(0, 35),
                 textcoords='offset points',horizontalalignment='center',
                 arrowprops=arrowprops)
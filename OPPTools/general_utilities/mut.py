""" Routines to replicate some Matlab functions and deal with with Matlab data.
"""

import numpy as np
import datetime as dt
import numpy_indexed as npi # conda install numpy-indexed -c conda-forge

def ismember(a, b):
    """ Elements in a that are in b
    
    Input:
        a,b     numpy arrays
        
    Output:
        tf      logical indices of elements in a which are in b
        locb    numeric indices of the corresponding elements in b
    
    Matlab's equivalent. No 'rows' option.
    """
    ashape = a.shape
    a = np.ravel(a)
    tf = np.in1d(a,b)
    locb = np.array([(np.where(b == i))[0][-1] if t else np.nan for i,t in zip(a,tf)])
    
    tf   = np.reshape(tf,  ashape)
    locb = np.reshape(locb,ashape)
    return tf,locb


def ismember_rows(a, b):
    """ Rows in a that are in b
    
    Output:
        tf      logical indices of rows in `a` which are in `b`
        locb    numerical indices of rows in `b` corresponding to rows in `a`; 
                such that a[locb,:] == b if all rows in `b` are present in `a`.
                -1 for rows in `a` which are not in `b`.
    """
    # Modified from here:
    # https://stackoverflow.com/questions/9269681/intersection-of-2d-numpy-ndarrays
    #Warning: view is a low-level operation. To work with transposed arrays, 
    # use a.T.copy() to re-write arrays in memory.
    a_view = a.view([('',a.dtype)]*a.shape[1])
    b_view = b.view([('',b.dtype)]*b.shape[1])
    tf = np.in1d(a_view, b_view)
    locb = np.array([(np.where(b == i))[0][-1] if t else np.nan for i,t in zip(a,tf)])
    
#    # faster alternative, locb is -1 for missing rows:
#    locb = npi.indices(a, b, missing=-1)
#    tf = locb!=-1
    
    return tf,locb


def intersect_rows(arr1, arr2):
    """ Common rows in two arrays
    
    From here:
    https://stackoverflow.com/questions/9269681/intersection-of-2d-numpy-ndarrays
    
    Warning: view is a low-level operation. To work with transposed arrays, 
    use a.T.copy() to re-write arrays in memory.
    
    Output:
        array of common rows (sorted)
    """
    arr1_view = arr1.view([('',arr1.dtype)]*arr1.shape[1])
    arr2_view = arr2.view([('',arr2.dtype)]*arr2.shape[1])
    intersected = np.intersect1d(arr1_view, arr2_view)
    return intersected.view(arr1.dtype).reshape(-1, arr1.shape[1])
#    # faster alternative, resulting array in different order:
#    return npi.intersection(arr1, arr2)


def struct_disp(s,indent_level=0,maxlen=None,usemaxlen=None):
    """ Display summary of a numpy struct or a dict returned by scipy.io.loadmat
    
    Parameters
    ----------
    
    s : dict or numpy struct
        Structure to display
    indent_level : int
        Used internally, only for recursive calls
    maxlen : int
        Used internally to get max name length for pretty printing
    usemaxlen : int
        Used to set width of the Names column; 
        by default the width is determined automatically from the max name length
    """
    indent = '  '*indent_level
    
    if maxlen is None:
        if usemaxlen is None:
            usemaxlen = struct_disp(s,maxlen=0)
        fstr = indent+'{:'+str(usemaxlen-len(indent))+'s}\t{:10s}\t{:25s}\t{}'
    else:
        maxleno = maxlen
    
    hdr = ['__header__', '__version__', '__globals__']
    
    if type(s) is dict: # top level returned by loadmat
        keys = s.keys()
    elif type(s) is np.void: # if numpy struct
        keys = s.dtype.names
    
    if maxlen is None:
        for k in keys:
            if k in hdr:
                print('{}\t{}'.format(k,s[k]))
        
        if indent_level==0:
            print(fstr.format('Name','Shape','Type','dtype'))
            print(fstr.format('----','-----','----','-----'))
    
    for k in keys:
        v = s[k]
        if k not in hdr: # len(k)<2 or k[:2]!='__':
            if maxlen is not None:
                maxleno = max(maxleno,len(k)+len(indent))
            if type(v) is not np.ndarray:
                size = len(v)
                type_k = type(v)
                dtype = ''
                is_struct = False
            else:
                size = v.shape
                if v.size!=0:
                    type_k = type(v[0])
                else:
                    type_k = type(v)
                if v.size!=0 and type(np.take(v,0)) is np.void: # if numpy struct
                    dtype = 'struct'
                    is_struct = True
                else:
                    dtype = v.dtype
                    is_struct = False
            
            if maxlen is None:
                print(fstr.format(k, str(size), str(type_k), str(dtype)))
            
            if is_struct:
                if maxlen is not None:
                    maxlenr = struct_disp(np.take(v,0),indent_level=indent_level+1,maxlen=maxleno)
                    maxleno = max(maxleno,maxlenr)
                else:
                    struct_disp(np.take(v,0),indent_level=indent_level+1,usemaxlen=usemaxlen)
    
    if maxlen is not None:
       return maxleno


def mtime2datetime(tm):
    """ Convert time in matlab datenum format to array of datetime objects
    """
    return np.array([dt.datetime.fromordinal(int(tk)) + 
                     dt.timedelta(days=tk%1) - 
                     dt.timedelta(days = 366) 
                     for tk in tm])
    
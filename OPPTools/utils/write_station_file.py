import OPPTools.fvcomToolbox as fvt
import numpy as np

def write_station_file(stations, gridfile, depthfile, prj, fname):
    tri, xy = fvt.readMesh_V3(gridfile)
    x, y = xy[:,0], xy[:,1]
    z = fvt.readBathy(depthfile)
    n=0
    print("Writing {} stations to {}".format(len(stations.keys()),fname))
    with open(fname,'w') as fout:
        fout.write("No       X              Y         Node      Depth (m)   Station Name\n")
        for stnID in stations.keys():
            n+=1
            stnName = stations[stnID]['Name']
            stnLon = stations[stnID]['Lon']
            stnLat = stations[stnID]['Lat']
            Sx, Sy = prj(stnLon, stnLat)
            d = np.sqrt( (Sx-x)**2 + (Sy-y)**2)
            node0 = np.argmin(d)  # node number, in Python 0-based indexing
            node1 = node0 + 1     # node number, in FORTRAN 1-based indexing
            nx = x[node0]
            ny = y[node0]
            dep = z[node0]
            #dist = d[node0]
            StnInfo = "'{:s} {:s}'".format(stnID, stnName)
            fout.write("{:2d}  {:12.6f} {:12.6f} {:6d} {:10.4f}      {:s}\n".format(n,nx,ny,node1,dep,StnInfo))

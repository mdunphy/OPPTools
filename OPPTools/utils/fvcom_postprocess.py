"""
FVCOM postprocessing tools.
"""

import glob
import os
import warnings
import bisect
import math
import time
import sys
from collections import namedtuple
import numpy as np
from datetime import datetime, timedelta
import netCDF4 as nc
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from OPPTools.mesh import mesh
import OPPTools.fvcomToolbox as fvt
#try:
#    import scDataset

def ncopen(ncf,casename=None):
    """Open netCDF file(s) as an instance of netCDF4.Dataset/MFDataset.
    
    Does nothing if an instance of netCDF4.Dataset/MFDataset is supplied.
    
    Input:
        ncf         options:
                        - a file name, str
                        - a list of file names
                        - a folder with a series of fvcom output files, str
                        - an instance of netCDF4.Dataset/MFDataset; 
                          in this case, ncf is returned without change
                          
        casename    an fvcom run case name used to search for the files;
                    used only if ncf is a folder
                    
    Output:
        an instance of netCDF4.Dataset/MFDataset
    """
    
    if type(ncf) in [nc.Dataset,nc.MFDataset]:
        return ncf
    else:
        try:
            ncf = nclist(ncf,casename)
        except RuntimeError as error:
            print ('Cannot open ',ncf)
            raise RuntimeError('An instance of netCDF4.Dataset/MFDataset, or a file name, a list of file names, or a folder must be supplied.')
#            return
            
        # open file(s) for reading
        if len(ncf)==1:
            try:
                return nc.Dataset(ncf[0]) # open model output file
            except RuntimeError:
                print('Cannot open '+ncf)
        else:
            try:
                return nc.MFDataset(ncf) # open series of model output files
            except RuntimeError as error:
                print ('Cannot open files.')
                print(error)


#scDataset -- Can be used instead of nc.MFDataset if handling of 
#             netcdf4 (non-Classic) files is needed. However, scDataset 
#             does not support variable attributes (needed in nctime).
#
#Dunphy, Michael
#To:
# O'Flaherty-Sproul, Mitchell‎; Blanken, Hauke‎; Krassovski, Maxim 
# 
#Friday, March 02, 2018 12:55 PM
#Hi,
#Here [1] is the “scDataset” that I was talking about yesterday. 
#Pass it a list of netcdf files (in chronological order) and you get a 
#nc.Dataset-like object that you can index as if it is one big file. 
#I’ve used it with split-up ROMS and NEMO outputs, 
#it should work for FVCOM outputs with little if any modifications.
# 
#I didn’t benchmark it against MFDataset because MFDataset refuses 
#to work with netcdf4 files (which was the motivation to build this thing). 
#I did benchmark it against xarray, which it outperforms by a large margin (below). 
#It would be interesting to benchmark it for split-up vs single netCDF3 files from FVCOM.
# 
#Cheers,
#M
# 
#[1] https://bitbucket.org/salishsea/analysis-michael/src/tip/scDataset/?at=default
# 
## Benchmark results at salish:
##
## Found 7 files
## scDataset , 27.58s, Peak Memory Usage 227.46 MB
## Found 7 files
## xarray    , 38.81s, Peak Memory Usage 19833.56 MB
#(from xarray_vs_scDataset.py)﻿


def nclist(fname,casename=None,sort_method='name'):
    """ Provide a list of FVCOM output files.
    
    Either search the specified folder for fvcom output files or ensure the 
    specified input is a list of files.
    
    Parameters
    ----------

    fname : str or list of str
        Options:    fvcom output file name, or
                    folder containing a series of output files, or
                    a list of output files
        Folder name can contain ** for recursive search in subfolders (requires 
        Python 3.5 or higher)
    casename : str (optional) 
        fvcom case name for the run to be used as part of the search pattern;
        if omitted, all files matching fvcom output file pattern are returned 
        providing they have same case name
    sort_method : str 
        'name' or 'time', key to use for sorting the output file list.
        Can also be 'none'.
        Sorting is done only if fname is a folder
                
    Outputs
    -------
    
    a list of file names
    """
    
#    try:
    if isinstance(fname, str):
        if os.path.isfile(fname):
            return [fname] # a single file was supplied; make a list and return
        
        elif os.path.isdir(fname) or '**' in fname:
            # a folder was supplied; list fvcom output files according to the pattern
    
            ext = '.nc'
            nserial = 4 # fvcom has 4-digit serial numbers for output files
            exclude = 'restart'
            
            # remove trailing separator if present
            if fname[-1]==os.sep:
                fname = fname[:-1]
            
            # list files matching the pattern
            if casename is None:
                ff = glob.glob(os.path.join(fname,'*'+'[0-9]'*nserial+ext))
                
                # range for case name
                if len(fname)==0:
                    istart = 0
                else:
                    istart = len(fname)+1
                    
                iend = -(nserial+len(ext)+1)
                
                # exclude files with casename ENDING with exclude pattern
                ff = [f for f in ff if f[iend-len(exclude):iend]!=exclude]
                
                casename = [f[istart:iend] for f in ff]
                casename_unique = list(set(casename))
                
                if len(casename_unique)>1:
                    #TODO: Proper exception handling
                    raise RuntimeError('Non-unique case names found: ',casename_unique)
            
            else: # casename was supplied
                ff = glob.glob(os.path.join(fname,casename+'_'+'[0-9]'*nserial+ext))
                
            if sort_method == 'name':
                ff.sort()
            elif sort_method == 'time':
                ff.sort(key=os.path.getmtime)
            elif sort_method == 'none':
                pass
            else:
                warnings.warn('sort_method should be "name", "time" or "none". Sorting was not performed.')
            
            return ff
            
    elif isinstance(fname, list):
#        # check if all files exist
#        notfound = [f for f in fname if not os.path.isfile(f)]
#        if len(notfound)>0:
#            #TODO: Proper exception handling
#            raise Exception('The following files were not found: ',notfound)
            
        return fname
    
    else:
        raise RuntimeError('A file name, list of file names, or folder must be supplied.')
#    except RuntimeError as error:
#           print ('Cannot list files in ',fname)
#           print(error)
#           return


def ncgrid(ncf):
    """Grid variables from an FVCOM output file.
    
    Input:
        ncf     either a file name or netCDF4.Dataset/MFDataset object (see ncopen)
    """
    
    ncf = ncopen(ncf)
        
    # nodes and depths
    x = ncf.variables['x'][:].astype('float64')
    y = ncf.variables['y'][:].astype('float64')
    h = ncf.variables['h'][:].astype('float64')
    
    if 'nv' in ncf.variables:
        # triangles
        tri = ncf.variables['nv'][:].T.copy() - 1 # 3-column, zero-based
        
        # element centroids and depths
        xc = np.mean(x[tri],axis=1)
        yc = np.mean(y[tri],axis=1)
        hc = np.mean(h[tri],axis=1)
    else: # e.g. station output file
        tri,xc,yc,hc = None,None,None,None
    
    with warnings.catch_warnings():
        # Ignore warnings from station output files:
        # UserWarning: WARNING: valid_min/valid_max not used since it 
        # cannot be safely cast to variable data type
        warnings.simplefilter("ignore", UserWarning)
        siglay = ncf.variables['siglay'][:].astype('float64')
        siglev = ncf.variables['siglev'][:].astype('float64')
    
    return x,y,h,tri,xc,yc,hc,siglay,siglev


def nctimelen(f):
    """ Length of time dimension of nc file object.
    
    Input:
        f       either a file name or netCDF4.Dataset/MFDataset object (see ncopen)
                
    Output:
        time  int, in datetime format
    """
    
    try:
        f = ncopen(f)
        
        if 'time' in f.dimensions:
            if type(f) is nc.Dataset:
                return f.dimensions['time'].size
            elif type(f) is nc.MFDataset:
                return f.dimensions['time'].dimtotlen
        else:
            warnings.warn('No time dimension in ',f.filepath())
            return None
        
    except RuntimeError as error:
           print(error)


def nctime(f,k=None):
    """ Time from nc file object.
    
    Input:
        f       either a file name or netCDF4.Dataset/MFDataset object (see ncopen)
        k       (optional) time step index/indices (zero-based) in the fvcom output;
                by default the entire time vector is returned
                
    Output:
        time    datetime object or numpy.ndarray of datetime objects
    """
    
    f = ncopen(f)
    
    if 'time' not in f.dimensions:
        warnings.warn('No time dimension in ',f.filepath())
        return None
    else:
        if k is None:
            k = np.arange(nctimelen(f))
        
        if 'Itime' in f.variables and 'Itime2' in f.variables:
            time = nc.num2date(f.variables['Itime'][k] + 
                               f.variables['Itime2'][k]/1000/60/60/24, 
                               f.variables['Itime'].units)
            
        elif 'Times' in f.variables:
            time = np.array([
                    datetime.strptime(
                            ''.join(f.variables['Times'][k1].astype(str)).strip(), 
                            '%Y-%m-%dT%H:%M:%S.%f') 
                    for k1 in k])
                    
        elif 'time' in f.variables:
            time = nc.num2date(f.variables['time'][k], f.variables['Itime'].units)
            
        else:
            raise RuntimeError('No time information is found.')
            
        return time
    

def ncextract(filelist,varlist_requested,fout):
    """ Extract specified varaibles from a list of nc files into a separate file.
    
    Files in the list must have same dimensions for the varaibles to extract
    (except the unlimited dimension).
    
    All static variables are also copied and that from the first file in the list.
    
    Parameters
    ----------
    
    filelist : str or list of str
        List of file names to read data from.
    varlist_requested : str or list of str
        List of names for the variables to extract. Should include only variables with time dimension.
        Time shouldn't be in the list. It is extracted by default.
    fout : str
        Name of the output file.
    """
    
    # ensure lists
    if type(filelist) is str: filelist = [filelist]
    if type(varlist_requested) is str: varlist_requested = [varlist_requested]
    
    d = nc.Dataset(filelist[0])
    var_available = d.variables.keys()
    
    # get static variables
    varstat = []
    dims = []
    for va in var_available:
        if 'time' not in d[va].dimensions:
            varstat.append(d[va].name)
            dims.extend(d[va].dimensions)
    
    # check if all the variables are present in the input files
    var_missing = [vk for vk in varlist_requested if vk not in var_available]
    if len(var_missing)>0:
        warnings.warn('The following variables are missing in the input file(s)')
    varlist = [vk for vk in varlist_requested if vk in var_available]
    if len(varlist)==0:
        warnings.warn('No variables to extract. Exiting.')
        return
    
    # list requested variable dimensions
    for vk in varlist:
        dims.extend(d[vk].dimensions)
    
    # put in same order as in the input file and exclude time
    dims = [k for k in d.dimensions if k in dims and k!='time']
    
    ncout = nc.Dataset(fout,'w',format='NETCDF3_CLASSIC')
    
    # create dimensions
    fvt.setup_time(ncout)
    for dim in dims:
        ncout.createDimension(d.dimensions[dim].name, d.dimensions[dim].size)
    
    # create variables
    for var in varstat+varlist:
        ncout.createVariable(var, d[var].datatype, d[var].dimensions)
        # copy variable attributes all at once via dictionary
        # https://stackoverflow.com/questions/15141563/python-netcdf-making-a-copy-of-all-variables-and-attributes-but-one
        ncout[var].setncatts(d[var].__dict__)
    
    # write static varaibles
    for var in varstat:
        ncout.variables[var][:] = d.variables[var][:]
    
    d.close()
    
    ncout.close()
    ncout = nc.Dataset(fout,'a',format='NETCDF3_CLASSIC')
    
    # go through files
    tcount = 0
    tst = time.time()
    for k,file in enumerate(filelist):
        src = nc.Dataset(file)
        
        if 'time' in src.dimensions:
            # time
            fvt.write_time(ncout,nctime(src),method='a') # append
            
            for var in varlist:
                # append
                nt = nctimelen(src)
                ncout.variables[var][tcount:tcount+nt,:] = src.variables[var][:]
            
            src.close()
            tcount += nt
        
        else:
            print('ncextract: NO time dimension in ',file,'; skipping.')
    
        pdone = (k+1)/len(filelist)
        eta = tst + (time.time()-tst)/pdone
        print('ncextract: ',file,'   ',pdone*100,'% done. ETA: ',time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(eta)))
        
    ncout.close()


def ncstation_names(ncf):
    """ Extract station names from a station output file
    
    """
    ncf = ncopen(ncf)
    
    stns = ncf['name_station'][:]
    stn = []
    for k in range(stns.shape[0]):
        stn.append(stns[k,:].tostring().decode().strip())
    
    return stn


def ncstation(f,var,stname=None,layer=None):
    """ Extract a variable time series for one station from a station output file
    
    layer : int or array_like of int
        zero-based index (indices) of layers to extract (for 3D vars)
    
    """
    t = ncstation_time(f)
    v = ncstation_var(f,var,stname=stname,layer=layer)
    return t,v


def ncstation_time(f):
    """ Extract time vector from a station output file
    """
    ncf = ncopen(f)
    tmjd = ncf['time'][:]
    return [datetime(1858,11,17,0,0,0) + timedelta(days=k) for k in tmjd]


def ncstation_var(f,var,stname=None,layer=None):
    """ Extract a variable for one station from a station output file
    
    layer : int or None, optional
        Zero-based index of a layer to extract (for 3D vars). 
        Default is None to extract all layers.
    """
    ncf = ncopen(f)
    
    # find requested stations
    stn = ncstation_names(ncf)
    if stname is None:
        ist = range(len(stn)) # all stations
    else:
        if type(stname) is str:
            stname = [stname] # ensure list
        try:
            # indices of requested stations in the list
            ist = [stn.index(k) for k in stname]
        except ValueError as error:
            errstr = 'Station '+str(error).split()[0]+ \
                     ' (and possibly others) is not in list of stations.'
            raise ValueError(errstr)
    
    # read data
    if ncf[var].ndim==2:
        v = ncf[var][:,ist]
    else:
        # requested layers
        if layer is None:
            layer = range(ncf[var].shape[1]) # all layers
        elif type(layer) is int:
            layer = [layer] # ensure list
        
        v = ncf[var][:,layer,ist]
    
    return v


def vel_snap(f,tstep,nanwet=True):
    """ 3D velocity field for one time step in FVCOM output.
    
    Inputs:
       f        either a file name or netCDF4.Dataset/MFDataset object (see ncopen)
        tstep   index of a time step in fvcom output
        nanwet  if True (default), place nans for dry cells (for runs with wetting/drying)
        
    Outputs:
        u,v     (nsiglay,nelem)     horizontal velocity components
        time    datetime
    """
    
    f = ncopen(f)
    
    # velocity components for the specified time step
    u = f.variables['u'][tstep,:]
    v = f.variables['v'][tstep,:]
    
    # invalidate dry cells
    if nanwet and ('wet_cells' in f.variables):
        wet = f.variables['wet_cells'][tstep,:].astype(bool)
        u[:,~wet] = np.nan
        v[:,~wet] = np.nan

    # extract time
    time = nctime(f,tstep)
    
    return u,v,time


def get_cmap_speed():
    cmap_file_name = 'cmap_speed.txt' # 3-column whitespace separated ascii file
    cmap_file = os.path.join(os.path.dirname(__file__),cmap_file_name)

    cm = np.genfromtxt(cmap_file)
    cmap = LinearSegmentedColormap.from_list('cmap_speed', cm)

    return cmap

def vel_quiver(x,y,tri,xc,yc,u,v,vscale=None,title=None,clim=None,iquiver=None,minarrow=None):
    """Velocity vectors on top of colour-coded speed field.
    
    Inputs:
        x,y     node coordinates
        tri     (nelem,3) triangles array
        xc,yc   (nelem,) vector origin coordinates, e.g. element centroids
        u,v     (nelem,) velocity components; can contain nans, e.g. for dry elements
        vscale  number of data units per arrow length unit
        title   title for the plot
        clim    [vmin,vmax]     limits for the colorscale; 
                                use None for vmin/vmax to set automatic limit
        iquiver (nelem,) bool   mask for elements to use for quiver plot;
                                e.g. arrows can be decimated using 
            iquiver = OPPTools.general_utilities.geometry.points_decimate(np.c_[xc,yc],r)
        minarrow    scalar  minimum speed constraint for arrows; used to avoid 
                    very small arrows or conversion of arrows to dots by plt.quiver
    """
    
    cmap = get_cmap_speed()

    fig, ax = plt.subplots(figsize=(18, 12))
    ax.set_aspect('equal')
    
#    ax.triplot(x,y,tri, color='0.8', zorder=1)
    
    a = (u**2 + v**2)**.5
    
    if minarrow is not None:
        # scale small arrows such that min amplitude is minarrow
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="divide by zero encountered in true_divide")
            warnings.filterwarnings("ignore", message="invalid value encountered in less")
            warnings.filterwarnings("ignore", message="invalid value encountered in multiply")
            
            minscale = minarrow/a
            minscale[minscale<1] = 1
            u = u*minscale
            v = v*minscale
    
    if clim is None:
        # entire speed range by default
        vmin = np.nanmin(a)
        vmax = np.nanmax(a)
    else:
        vmin = clim[0]
        vmax = clim[1]
    
    # colour-coded speed field
    tpc = ax.tripcolor(x,y,tri,a,cmap=cmap, zorder=2, vmin=vmin, vmax=vmax)
    tpc.cmap.set_under('0.8') # colour for nans
    cbar = fig.colorbar(tpc)
    cbar.ax.set_ylabel('Speed (m/s)')

    # quiver plot
    if iquiver is None:
        iquiver = np.zeros(len(xc), dtype=bool) # plot all arrows by default
    
    # for scale_units='xy' and scale other than 1, quiverkey will not give correct arrow length;
    # use inches to make arrow size consistent across different plots
    
    quiv = ax.quiver(xc[iquiver],yc[iquiver], u[iquiver],v[iquiver],
                     units='inches', scale_units='inches', scale=vscale,
                     headwidth=2, headlength=3.2, headaxislength=3,
                     zorder=3)
    # additional params controlling arrow appearance:
    # color='blue', width=0.007,
    
#    print(quiv.width, quiv.headwidth, quiv.headlength, quiv.headaxislength, quiv.minshaft, quiv.minlength)
    
    ax.quiverkey(quiv, 0.93, 0.93, 2, '2 m/s', labelpos='N', coordinates='axes')
    
    if title is not None:
        ax.set_title(title)
    
    return fig,ax,tpc,quiv
    
    
def average_zrange(zsiglevc,zrange,from_bottom=False):
    """ Weights for averging values at sigma-layers within the specified depth range.
    
    Perform averaging of a variable v(nsiglay,d1,...) as:
        va = np.sum(v*w,axis=0)
    
    Inputs:
        zsiglevc (nsiglev,d1,...) depths of sigma-levels (z-axis positive up) at
                                  locations of the variable to average, 
                                  i.e. at elements for velocity and at nodes for T,S
        zrange   (2,)             [upper,lower] depth limits for averaging 
                                  (z-axis positive up);
                                  if from_bottom is True, z-axis is positive down, 
                                  i.e. to get averages in 10 m range from bottom 
                                  zrange=[0, -10]
        from_bottom     bool      if True, count the depth range from the bottom
                                  if False (default), count from the surface
    Outputs:
        w        (nsiglay,d1,...) normalized weights for averaging within the range
        
    Examples:
        import netCDF4 as nc
        import OPPTools.fvcomToolbox as fvt
        f='/media/krassovskim/MyPassport3/fvcom/opp/run021/a/vhfr_low_v2_0005.nc'
        ncf = nc.Dataset(f)
        
        hc = ncf.variables['h_center'][:]
        siglevc = ncf.variables['siglev_center'][:]
        tri = ncf.variables['nv'][:]
        
        # for 2D arrays
        u = ncf.variables['u'][0,:,:] # 1st time step
        zeta = ncf.variables['zeta'][0,:] # 1st time step
        zetac = fvt.convertNodal2ElemVals(tri.T, zeta)
        zsiglevc = (hc+zetac)*siglevc
        
        # averages in 10-m range from surface:
        w = fpp.average_zrange(zsiglevc,[0., -10.])
        uas = np.sum(u*w,axis=0)
        
        # averages in 10-m range from bottom:
        w = fpp.average_zrange(zsiglevc,[0., -10.],True)
        uab = np.sum(u*w,axis=0)
        
        # for 3D arrays
        u = ncf.variables['u'][:]       # (time,siglay,nelem)
        zeta = ncf.variables['zeta'][:] # (time,nodes)
        u = np.moveaxis(u, [0, 1, 2], [2, 0, 1]) # (siglay,nelem,time)
        zeta = np.moveaxis(zeta, [0, 1], [1, 0]) # (nodes,time)
        zetac = fvt.convertNodal2ElemVals(tri.T, zeta)
        zsiglevc = (hc[:,None]+zetac)[None,:,:]*siglevc[:,:,None] # (siglay,nelem,time)
        
        # averages in 10-m range from surface:
        w3 = fpp.average_zrange(zsiglevc,[0., -10.])
        ua3 = np.sum(u*w3,axis=0)
        
        np.nanmax(np.abs(ua3[:,0]-uas)) # 0.0
    """
    
    if from_bottom:
        zsiglevc = np.min(zsiglevc,axis=0) - zsiglevc
        dsign = -1
    else:
        dsign = 1
    
    dzu = zsiglevc - zrange[0]
    dzl = zsiglevc - zrange[1]
    invl = dzl<0 # out of range layers based on lower limit
    invu = dzu>0 # out of range layers based on upper limit
    inv = np.logical_or(invl,invu) # out of range layers
    dzu[inv] = 0 # discard out of range layers
    dzl[inv] = 0
    dzu = np.diff(dzu, axis=0) # layer thickness including those partially in range
    dzl = np.diff(dzl, axis=0)
    dz = np.minimum(dsign*dzu,dsign*dzl) # doesn't cover one-layer ranges: contain all zeros
    
    # one-layer ranges have change of sign along dimension 0
    ilu = np.sign(np.diff(invu.astype(int),axis=0)) != 0
    ill = np.sign(np.diff(invl.astype(int),axis=0)) != 0
    il = np.logical_or(ill,ilu)
    il[:,np.any(dz!=0,axis=0)] = 0 # zero out for nodes that span at least two layers
    dz[il] = -1.
    
    # normalize weights to sum up to 1 for each node
    with np.errstate(invalid='ignore'): # suppress division by zero warning
        w = dz/np.sum(dz,axis=0)
        
    return w


def vertical_transect(xt,yt,x,y,tri,h,siglay,siglev,xc=None,yc=None,nbe=None):
    """ Vertical transect of a triangular grid along a piecewise-linear line [xt,yt]
    for tracer (node-based) and velocity (element-based) quantity.
    
    Horizontal resolution of the resulting transect is defined by the points of
    intersection with the grid edges; vertical resolution is defined by the model
    sigma-layers.
    
    Inputs:
        xt,yt   (nt,)   coordinates of the transect line
        x,y     (nn,)   model grid nodal coordinates
        tri     (ne,3)  triangulation array, zero-based
        h       (nn,)   model depths
        siglay  (nn,ns) sigma-layers
        siglev  (nn,nslv)   sigma-levels
        xc,yc   (ne,)   grid element centroids
        nbe     (ne,3)  elements surrounding each element (indices into rows of tri);
                        can be obtained from the fvcom output variable 'nbe' or by tri_nbe();
                        contains 1 zero value for each triangle with a boundary edge
      
      Model grid metrics can be obtained with 
      [x,y,h,tri,ns,siglay,xc,yc,nbe] = fvcom_ncgrid(outfile)
      
    Output:
        tr  named tuple with fields:
            d       (nt,)   distance along the transect
            h       (nt,)   bathymetry along the transect
            siglay  (nt,ns) model sigma layers along the transect
            siglev  (nt,nl) model sigma levels along the transect
            x,y     (nt,)   transect point coordinates
            nidn,wn (nt,3)  interpolant to interpolate scalar fields to the 
                            transect for node-based variables:
                                nidn    indices into grid nodes
                                wn      corresponding weights
            nide,we (nt*2,4) same for element-based varaibles, but
                             nide are indices into grid elements
                
            theta   (nt*2,) transect direction
            
            nt is number of transect points; for element-based varaibles, 
            points are duplicated to reflect discontinuities at the element boundaries.
    """
        
    # ensure dimensions (node, nsiglay) for siglay
    if np.ndim(siglay)==1 or siglay.shape[1]==1:
        nn = len(h)
        siglay = np.tile(siglay.squeeze(),(nn,1))
    elif siglay.shape[1]==len(x):
        siglay = siglay.T.copy()
        
    # ensure dimensions (node, nsiglev) for siglev
    if np.ndim(siglev)==1 or siglev.shape[1]==1:
        nn = len(h)
        siglev = np.tile(siglev.squeeze(),(nn,1))
    elif siglev.shape[1]==len(x):
        siglev = siglev.T.copy()
        
    # transect with interpolant
    xi,yi,nidn,wn,xie,yie,nide,we,theta = mesh.tri_transect(xt,yt,x,y,tri,xc,yc,nbe)
    
    # for node-based quantities
    dn = np.r_[0, np.cumsum(np.sqrt(np.diff(xi)**2 + np.diff(yi)**2))] # distances along the transect
    hn = mesh.tri_interp_eval(nidn,wn,h, zero_allnans=True) # bathy profile
    siglayn = mesh.tri_interp_eval(nidn,wn,siglay, zero_allnans=True) # sigma layers on the transect
    siglevn = mesh.tri_interp_eval(nidn,wn,siglev, zero_allnans=True)
    
    fvcom_transect = namedtuple('fvcom_transect', 'd h siglay siglev x y nidn wn nide we theta')    
    
    return fvcom_transect(dn,hn,siglayn,siglevn,xi,yi,nidn,wn,nide,we,theta)


def vertical_transect_snap(f,ktime,varname,tr):
    """ Generate inputs for transect plotting of a snapshot of fvcom output.
    
    Generate inputs to vertical_transect_plot().
    
    Inputs:
        f       either a file name or netCDF4.Dataset/MFDataset object (see ncopen)
        ktime   int     time series index (zero-based) of the output series
        varname str     standard fvcom variable or 'normal velocity' or 'tangential velocity';
                        normal velocity is positive into the page
                        tangential velocity is positive from lower to higher d
        tr      named tuple     output from vertical_transect()
    """

    f = ncopen(f) # ensure netCDF4.Dataset/MFDataset
    varname = varname.lower()
    
    if varname in ['normal velocity','tangential velocity']:
        fvarname = 'v'
        longname = varname
        v = f['v'][ktime,:,:].T.copy()
        u = f['u'][ktime,:,:].T.copy()
        vi = mesh.tri_interp_eval(tr.nide, tr.we, v)
        ui = mesh.tri_interp_eval(tr.nide, tr.we, u)
        vn,ut = normal_vel(ui,vi,tr.theta[:,None])
        if varname == 'normal velocity':
            vi = vn
        else:
            vi = ut
            
        # normal/tangential velocity depends on theta; 
        # duplicate points where theta changes, so values with different theta 
        # are not averaged subsequently
        dth = np.r_[np.diff(tr.theta)!=0, False]
        # we need only diffs for same-location points
        dth[1::2] = False # zero out theta diffs between different points
        dth[np.where(dth)[0]+1] = True # include next node for duplication as well
        nrepeat = dth.astype(int) + 1
        vi = np.repeat(vi,nrepeat,axis=0)
        nrepeatd = nrepeat[::2] # repeats for dd and sigz
        
    else: # a variable independent of transect orientation
        fvarname = varname # all fvcom vars are lowercase
        longname = getattr(f[fvarname],'long_name')
        v = f[fvarname][ktime,:,:].T.copy()
        # get the right interpolant
        if 'node' in f[fvarname].dimensions:
            nid = tr.nidn
            w = tr.wn
        elif 'nele' in f[fvarname].dimensions:
            nid = tr.nide
            w = tr.we
        vi = mesh.tri_interp_eval(nid, w, v) # interpolate to transect
        nrepeatd = None
    
    if 'nele' in f[fvarname].dimensions:
        # average at discontinuities for element-based quantities;
        # all points are duplicated (see mesh.tri_transect), so average all pairs
        vi = np.stack((vi[:-1:2,:],vi[1::2,:]), axis=2)
        inv = np.all(np.isnan(vi),axis=2) # nansum gives 0 for all-nan slices; make them nans
        vi = np.nansum(vi, axis=2)/2
        vi[inv] = np.nan
    
    nd = len(tr.d)
    ns = tr.siglay.shape[1]
    if vi.shape[1] == ns: # siglay-based variable
        # duplicate 1st and last layers to extend data to surface and bottom
        vi = np.c_[vi[:,0],vi,vi[:,-1]]
        sig = np.c_[np.zeros(nd),tr.siglay,-np.ones(nd)] # mid-layer depths
        ns += 2
    elif vi.shape[1] == ns+1: # siglev-based variable
        sig = tr.siglev
        ns += 1
    
    zeta = f['zeta'][ktime,:]
    zeta = mesh.tri_interp_eval(tr.nidn, tr.wn, zeta, zero_allnans=True)
    zsig = zeta[:,None] + (tr.h+zeta)[:,None]*sig # layer bound depths
    dd = np.tile(tr.d[:,None],(1,ns))
    
    if nrepeatd is not None:
        dd = np.repeat(dd,nrepeatd,axis=0)
        zsig = np.repeat(zsig,nrepeatd,axis=0)
    
    timestr = datetime.strftime(nctime(f,ktime), ' %Y-%m-%d %H:%M UTC')
    units = getattr(f[fvarname],'units')
    if units=='meters s-1':
        units='m/s'
    elif units=='1e-3':
        units='psu'
    
    # Remove leading and trailing outside-of-the-grid parts
    ival = np.where(~np.all(np.isnan(vi),axis=1))[0]
    # before the 1st valid value and after the last valid value
    inv = np.r_[np.arange(ival.min()),np.arange(ival.max()+1,dd.shape[0])]
    dd = np.delete(dd,inv,axis=0)
    zsig = np.delete(zsig,inv,axis=0)
    vi = np.delete(vi,inv,axis=0)
    
    return dd,zsig,vi,longname,units,timestr
    

def vertical_transect_snap_all(f,varname,tr):
    t = nctime(f)
    nt = len(t)
    dd,zsig0,v0,longname,units,_ = vertical_transect_snap(f,0,varname,tr)
    zsig = np.zeros(zsig0.shape+(nt,)) # preallocate (nsig,ndist,ntime)
    v    = np.zeros(zsig0.shape+(nt,))
    zsig[:,:,0] = zsig0
    v[:,:,0] = v0
    
    tst = time.time()
    for k in range(1,nt):
        _,zsig[:,:,k],v[:,:,k],_,_,_ = vertical_transect_snap(f,k,varname,tr)
        pdone = (k+1)/nt
        eta = tst + (time.time()-tst)/pdone
        print('Evaluating transect: ',pdone*100,'% done. ETA: ',time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(eta)))
    
    return t,dd,zsig,v,longname,units


def vertical_transect_plot(dd,zsig,vi,fvcom_varname,varname,units,timestr,
                           plot_grid=True,
                           contour_interval=None,ncontours=None,
                           return_handles=False):
    """ Plot transect obtained with vertical_transect_snap()
    
    Inputs:
        dd      (nh,nv) horizontal coordinates for plt.colormesh
        zsig    (nh,nv) vertical coordinates for plt.colormesh
        vi      (nh,nv) variable values at the transect for plt.colormesh
        fvcom_varname str     var name in fvcom output file
        varname str     var name for the title
        units   str     units for the variable
        timestr str     time of the snapshot for the title
        plot_grid bool  whether to plot vertical and horizontal grid lines on the transect;
                        default is True
        contour_interval    a scalar    (optional) interval for contouring the data
        ncontours           a scalar    (optional) maximum number of interval for 
                                        contouring the data; if supplied the contour
                                        interval is determined automatically
        return_handles  bool    If True, return fig,ax,pcolormesh, and colorbar handles
        
        If either contour_interval or ncontours are supplied, the colormap is 
        descrete, otherwise the colormap is continuous.
    """
    
    fig,ax = plt.subplots()
    
    # property plot
    if fvcom_varname.lower() in ['u','v','ww','omega','normal velocity','tangential velocity']:
        clim = np.max(np.abs(vi))
        vmin,vmax = -clim,clim
        cmapname = 'bwr'
    else:
        vmin,vmax = np.nanmin(vi), np.nanmax(vi)
        cmapname = 'viridis'
    
    if contour_interval is None and ncontours is None: # use continuous colormap with pcolormesh
        hp = ax.pcolormesh(dd,zsig,vi,shading='gouraud',vmin=vmin,vmax=vmax,cmap=cmapname)
        # pcolormesh with shading='gouraud' is useless with descrete colormaps
    else: # use discrete colormap with contourf
        if ncontours:
            contour_interval = best_tick(vmax-vmin, ncontours)
        cticks = np.arange(np.floor(vmin/contour_interval),
                           np.ceil(vmax/contour_interval)+1) * contour_interval
        # https://stackoverflow.com/questions/14777066/matplotlib-discrete-colorbar
        cmap = plt.cm.get_cmap(cmapname)
        norm = mpl.colors.BoundaryNorm(cticks, cmap.N)
        hp = ax.contourf(dd,zsig,vi,levels=cticks,cmap=cmap,norm=norm)
    
    cbar = fig.colorbar(hp) #, ax=ax
    cbar.ax.set_ylabel(varname+' ('+units+')')
    plt.xlabel('Distance (m)')
    plt.ylabel('Depth (m)')
    ax.set_title(varname+'  '+timestr)
    
    # vertical and horizontal grid
    if plot_grid:
        # combine the grid from individual lines separated with nans
        dh=np.r_[dd,np.ones((1,dd.shape[1]))*np.nan] # horiz
        dv=np.c_[dd,np.ones((dd.shape[0],1))*np.nan] # vert
        zh=np.r_[zsig,np.ones((1,zsig.shape[1]))*np.nan] # horiz
        zv=np.c_[zsig,np.ones((zsig.shape[0],1))*np.nan] # vert
        
        d = np.r_[dh.T.ravel(), dv.ravel()]
        z = np.r_[zh.T.ravel(), zv.ravel()]
        
        ax.plot(d,z,color='0.5',linewidth=0.3,antialiased=True)
    
    if return_handles:
        return fig,ax,hp,cbar


def normal_vel(u,v,theta):
    """ Velocity rotated by angle theta (rad).
    u,v     eastward and northward components
    theta   transect angle, -pi to pi, positive CCW from x-direction
    vn,ut   components normal and tangential to transect line with direction theta
    """

    vn = v*np.cos(theta) - u*np.sin(theta) # normal component
    ut = v*np.sin(theta) + u*np.cos(theta) # tangential component
    return vn,ut # normal and tangential components


def best_tick(span, mostticks):
    # https://stackoverflow.com/questions/361681/algorithm-for-nice-grid-line-intervals-on-a-graph
    minimum = span / mostticks
    magnitude = 10 ** math.floor(math.log(minimum, 10))
    residual = minimum / magnitude
    # this table must begin with 1 and end with 10
    table = [1, 2, 5, 10] # options for gradations in each decimal interval
    tick = table[bisect.bisect_right(table, residual)] if residual < 10 else 10
    return tick * magnitude


def plot_relative_ssh(ncf,k,irel,x,y,tri):
    """ Plot tripcolor of ssh at time step `k` in nc file relative to node `irel`.
    """
    h=ncf['zeta'][0,:]
    hr = h - h[irel]
    
    plt.figure(figsize=(11,8.5))
    plt.tripcolor(x,y,tri,hr,vmin=-.05,vmax=.05)
    plt.colorbar()
    plt.tight_layout()
#    plt.savefig("image_{:03d}.png".format(frame_number))
    

from matplotlib.animation import FuncAnimation
import matplotlib
def animate_relative_ssh(f,irel):
    # https://community.dur.ac.uk/joshua.borrow/blog/posts/making_research_movies_in_python/
    
    def animate(frame, ax,ncf,irel,x,y,tri):
        """
        Animation function. Takes the current frame number (to select the potion of
        data to plot) and a line object to update.
        """
        print(frame)
        # Not strictly neccessary, just so we know we are stealing these from
        # the global scope
#        global x,y,tri
        
        # We want up-to and _including_ the frame'th element
        h = ncf['zeta'][frame,:]
        hr = h - h[irel]
        nctime(ncf,k=frame)
        
        # find existing tripcolor
        line = [l for l in ax.get_children() if type(l) is matplotlib.collections.PolyCollection]
        # remove
        if len(line)==1:
            line[0].remove()
        del line
        #create second plot
        line = ax.tripcolor(x,y,tri,hr,vmin=-.02,vmax=.02,cmap=plt.get_cmap('seismic'))
    
        # This comma is necessary!
        return (line,)
    
    ncf=nc.Dataset(f)
    x=ncf['x'][:]
    y=ncf['y'][:]
    tri=ncf['nv'][:]-1
    tri=tri.T # (nelem,3)
    
    h=ncf['zeta'][0,:]
    hr = h - h[irel]
    
    # Some global variables to define the whole run
    total_number_of_frames = ncf.dimensions['time'].size
    
    
    
    # Now we can do the plotting!
    fig, ax = plt.subplots(1)
    fig.set_size_inches(11,8.5)
    
    # Initialise
    line = ax.tripcolor(x,y,tri,hr,vmin=-.02,vmax=.02,cmap=plt.get_cmap('seismic'))
    fig.colorbar(line)
    fig.tight_layout()
    
    fps = 1
    
    animation = FuncAnimation(
        # Your Matplotlib Figure object
        fig,
        # The function that does the updating of the Figure
        animate,
        # Frame information (here just frame number)
#        np.arange(total_number_of_frames),
        np.arange(35,72), ########## 2nd half
        # Extra arguments to the animate function
        fargs=[ax,ncf,irel,x,y,tri],
        # Frame-time in ms; i.e. for a given frame-rate x, 1000/x
        interval = 1000/fps
    )
    animation.save("run031f.mp4")
import numpy as np
import netCDF4 as nc
import OPPTools.fvcomToolbox as fvt
from OPPTools.utils import fvcom_postprocess as fpp


def nc_surface_extract(infile, outfile, zrange=[0, -8]):
    """
    Copies the input file to output file while retaining only the surface
    layer for variables that have a depth dimension
    
    zrange  identifies the layer to extract:
            zrange=0    extract only the surface sigma-layer
            zrange=[upper,lower]  average within the depth range, e.g. 
            zrange=[0., -8.] (default) to average within 8 m from the surface
    """
    fin = nc.Dataset(infile, 'r')
    fout = nc.Dataset(outfile, 'w', format=fin.data_model)
    
    if zrange != 0: # need to average in the depth range
        h = fin.variables['h'][:]
        siglev = fin.variables['siglev'][:]
        zeta = fin.variables['zeta'][:] # (time,nodes)
        zeta = np.moveaxis(zeta, [0, 1], [1, 0]) # (nodes,time)
        zsiglev  = (h[:,None]+zeta)[None,:,:]*siglev[:,:,None] # (siglay,nnode,time)
        wn = fpp.average_zrange(zsiglev,zrange) # weights for node-based variables
        
        hc = fin.variables['h_center'][:]
        siglevc = fin.variables['siglev_center'][:]
        tri = fin.variables['nv'][:]
        zetac = fvt.convertNodal2ElemVals(tri.T, zeta)
        zsiglevc = (hc[:,None]+zetac)[None,:,:]*siglevc[:,:,None] # (siglay,nelem,time)
        we = fpp.average_zrange(zsiglevc,zrange) # weights for element-based variables

    # Copy global attributes
    for attr in fin.ncattrs():
        fout.setncattr(attr, fin.getncattr(attr))

    # Copy dimensions except siglev and siglay
    for key in fin.dimensions.keys():
        dim = fin.dimensions[key]
        if dim.name in ['siglay', 'siglev']:
            #print("Not creating dimension {}".format(dim.name))
            continue
        elif dim.name in ['time']:
            #print("Creating dimension time with size unlimited")
            fout.createDimension(dim.name, None)
        else:
            #print("Creating dimension {} with size {}".format(dim.name, dim.size))
            fout.createDimension(dim.name, dim.size)

    # Copy variables
    for k, v in fin.variables.items():
        if k in ['siglay', 'siglev', 'siglay_center', 'siglev_center',
                 'viscofm','viscofh','km','kh','kq','q2','q2l','l','omega']:
            # Skip vertical dim variables, viscosity, etc
            continue
        elif 'siglay' in v.dimensions:
            if zrange==0:
                # Copy only the surface layer
                #print("Extract surface for {} with dims {}".format(k, v.dimensions))
                tr = tuple([slice(None) if q not in ['siglay'] else slice(0, 1) for q in v.dimensions]) # , 'siglev'
                val = fin.variables[v.name][tr]
                long_name_suffix = '; surface'.format(zrange[0],zrange[1])
            else:
                # average within the range
                val = np.moveaxis(v[:], [0, 1, 2], [2, 0, 1]) # (time,siglay,nele) -> (siglay,nele,time)
                if 'nele' in v.dimensions:
                    w = we # element-based
                else:
                    w = wn # node-based
                val = np.sum(val*w,axis=0).T # (nele,time) -> (time,nele)
                long_name_suffix = '; {} to {} m average'.format(abs(zrange[0]),abs(zrange[1]))
            
            # Create new output variable, copy attributes
            newdims = [q for q in v.dimensions if q not in ['siglay']] # , 'siglev'
            fout.createVariable(v.name, v.datatype, newdims)
            for attr in v.ncattrs():
                atval = v.getncattr(attr)
                if attr=='long_name':
                    atval = atval+long_name_suffix
                elif attr=='coordinates':
                    atval = atval.replace(' siglay','')
                fout.variables[v.name].setncattr(attr, atval)
                
            fout.variables[v.name][:] = val
            
        else:
            # Create identical output variable, copy attributes and data
            #print("Direct copy of {} with dims {}".format(k, v.dimensions))
            fout.createVariable(v.name, v.datatype, v.dimensions)
            for attr in v.ncattrs():
                fout.variables[v.name].setncattr(attr, v.getncattr(attr))
            fout.variables[v.name][:] = fin.variables[v.name][:]

    fin.close()
    fout.close()
    
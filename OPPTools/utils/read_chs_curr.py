import numpy as np

def read_chs_curr(currfile):
    """ Read CHS tidal constituents file for current velocitiy (const.curr format)
    """
    
    with open(currfile,'r') as fid:
        hdr = []
        line = fid.readline()
        while '|' in line:
            hdr.append(line)
            line = fid.readline()
        
        d = [line.strip().split()]
        for line in fid:
            d.append(line.strip().split())
        
        cname = np.array([str(k[0]) for k in d])
        with np.errstate(divide='ignore'):
            freq = np.array([np.divide(1,float(k[1])) for k in d]) # cph
        amaj = np.array([float(k[2]) for k in d])
        amin = np.array([float(k[3]) for k in d])
        inclination = np.array([float(k[4]) for k in d])
        phase = np.array([float(k[5]) for k in d])
    
    return cname,freq,amaj,amin,inclination,phase,hdr

#% CurrentConstit  04100 SECOND NARROWS                               2011/06/21||
#% !Computed    49 17.6800N 123  1.4700W                        +08   0700:00   ||
#%         64 0000days 100.0% R                                       0000:00 08||
        
#meta.id = hdr{1}(17:21);
#meta.stname = deblank(hdr{1}(23:67));
#meta.lat = (str2double(hdr{2}(13:15)) + str2double(hdr{2}(17:23))/60) * ((hdr{2}(24)=='N')*2-1);
#meta.lon = (str2double(hdr{2}(26:28)) + str2double(hdr{2}(30:35))/60) * ((hdr{2}(37)=='E')*2-1);
#meta.reclength = str2double(hdr{3}(12:15));
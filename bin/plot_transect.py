"""
Example of plotting a vertical transect of an fvcom output field.
"""
import numpy as np
from OPPTools.utils import fvcom_postprocess as fpp

fname = '/data/opp/vhfr/run034/a/vh_x2_0001.nc'
ktime = 23
xt = np.genfromtxt('test_poly.txt',delimiter=',',usecols=0)
yt = np.genfromtxt('test_poly.txt',delimiter=',',usecols=1)

# standard fvcom variable or 'normal velocity' or 'tangential velocity'
#varname = 'salinity'
#varname = 'temp'
#varname = 'v'
#varname = 'normal velocity'
#varname = 'tangential velocity'
#varname = 'omega'
testvars = ['salinity','v','normal velocity','omega']
# node-based, element-based, derived, siglev-based
testvars = ['salinity']
x,y,h,tri,xc,yc,_,siglay,siglev = fpp.ncgrid(fname)
tr = fpp.vertical_transect(xt,yt,x,y,tri,h,siglay,siglev)

for varname in testvars:
    dd,zsig,vi,longname,units,timestr = fpp.vertical_transect_snap(fname,ktime,varname,tr)
    fpp.vertical_transect_plot(dd,zsig,vi,varname,longname.capitalize(),units,timestr)
    fpp.vertical_transect_plot(dd,zsig,vi,varname,longname.capitalize(),units,timestr,plot_grid=False)
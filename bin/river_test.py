"""
Tests to create fvcom river forcing for muti-day and single-day runs from NEMO river forcing.
"""

import os
import numpy as np
import glob
import OPPTools.fvcomToolbox as fvt
import OPPTools.river as riv

# data files and folders
opp_work = '/media/krassovskim/MyPassport/Max/Projects/opp'
opp_data = '/media/krassovskim/MyPassport/Data/opp'
# folder with NEMO river forcing files upto 2018-11-28
nemo_river_folder = os.path.join(opp_data,'rivers_nemo/2018-08')
nemo_river_search_pattern = 'R201702DFraCElse_y*.nc'
nemo_coord_file = os.path.join(opp_data,'coordinates_seagrid_SalishSea201702.nc')
river_constemp_file = os.path.join(opp_data,'rivers_nemo/rivers-climatology/rivers_ConsTemp_month.nc')

fraser_nodes_file = os.path.join(opp_work,'vh_x2_river_nodes_fraser.txt')
grid = 'vh_x2'

# start and end of the run
#timelim = ['2018-11-29 00:00:00','2018-11-30 00:00:00'] # one-day, e.g. forecast
#tag = '2018-11-29_test_1day_extrap'
timelim = ['2018-11-22 00:00:00','2018-11-30 00:00:00'] # multi-day, e.g. spinup
tag = '2018-11-22_test_1week'

# files to generate
casename = os.path.join(opp_work,grid+'_'+tag)
namelist_file = os.path.join(opp_work,grid+'_namelist_'+tag+'.river')

# get available data files
# needs Python 3.5 or newer: ** and recursive=True are for recursive search
nemo_river_files = glob.glob(
        os.path.join(nemo_river_folder,'')+'**/'+nemo_river_search_pattern, 
        recursive=True)

if (len(nemo_river_files)==0):
    raise ValueError('No '+nemo_river_search_pattern+' river forcing files in '+nemo_river_folder)

## alternatively, supply a one-file list
#nemo_river_file = [os.path.join(nemo_river_folder,'R201702DFraCElse_y2018m11d28.nc')]

time,q,temp = riv.nemo_fraser(timelim,
                              nemo_river_files,
                              nemo_coord_file,
                              river_constemp_file)
# time should cover the entire interval of the model run
time_str = [t1.strftime('%Y-%m-%d %H:%M:%S') for t1 in time]

# distribute over FVCOM river inflow nodes
rivLoc = np.genfromtxt(fraser_nodes_file,dtype=int) # 1-based fvcom node indices
nriv = len(rivLoc)
qdis = riv.discharge_split(q,nriv) # (ntimes, nRiv)
temp = np.tile(temp[:,None],nriv) # replicate temperature

# write forcing file
fvt.generate_riv(casename,time_str,rivLoc,qdis,
                 temp=temp,
                 rivName='fraser',
                 namelist_file=namelist_file)
"""
Surface velocity plots for fvcom output.
"""

import warnings
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
from OPPTools.utils import fvcom_postprocess as fpp
from OPPTools.general_utilities import geometry as mgeom

# Location of fvcom output
# fname = '/data/opp/vhfr/run034/a' # entire folder (for a series of files)
fname = '/data/opp/vhfr/run034/a/vh_x2_0005.nc' # one file
title = 'Surface velocity'
savename = '/media/krassovskim/MyPassport/Max/Projects/opp/eps/vel_quiver' # path + name prefix

clim = [0, 3] # m/s; fixed colorscale limits
vscale = 8 # for units='inches' in vel_quiver: 1 m/s arrow has length of 1/vscale inches
minarrow = 0.4 # m/s; to avoid very small arrows or conversion of arrows to dots by plt.quiver
dthres = 100 # decimation radius for arrows (m)
dt = 1 # hour; do plots every dt hours of simulation time

# zoom on these areas
# [xmin,xmax,ymin,ymax] in fvcom grid coordinates
boxes = 1e6*np.array([
[0.480261433177043,   0.490323236054385,   5.457552054347616,   5.465487895649262],
[0.488304206408948,   0.500775675455476,   5.458787489946255,   5.463835980153344],
[0.496999407370256,   0.509470876416784,   5.459221715399989,   5.466639337009188],])

boxtag = ['englishbay','harbour','indianarm']


f = fpp.ncopen(fname)
x,y,h,tri,xc,yc,hc,siglay,_ = fpp.ncgrid(f)
ntime = fpp.nctimelen(f)
if ntime>1:
    dto = (fpp.nctime(f,1) - fpp.nctime(f,0)).seconds /3600 # output time interval
    step = np.round(dt/dto).astype(int) # stepping over times
else:
    step = 1

for kt in range(0,ntime,step):
    u,v,time = fpp.vel_snap(f,kt)
    
    iquiver = mgeom.points_decimate(np.c_[xc,yc],dthres)
    
    fig,ax,tpc,quiv = fpp.vel_quiver(x,y,tri,xc,yc,u[0,:],v[0,:],vscale=vscale,
                        title=title+datetime.strftime(time, ' %Y-%m-%d %H:%M UTC'),
                        clim=clim,
                        iquiver=iquiver,
                        minarrow=minarrow)
    
    timetag = datetime.strftime(time, '_%Y-%m-%d_%H-%M')
    
    for kb in range(np.shape(boxes)[0]):
        ax.set_xlim(boxes[kb,:2])
        ax.set_ylim(boxes[kb,2:])
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="invalid value encountered in less")
            fig.savefig(savename+'_'+boxtag[kb]+timetag+'.png')
    
    plt.close(fig)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 15:51:30 2018

Example of generation of ITS fields from NEMO and climatology.
"""
print('starting gen_its2...')

import sys
import time
import OPPTools.fvcomToolbox as fvt
import OPPTools.nesting as nesting
#from OPPTools.nesting import its_from_nemo, its_from_jakes_climatology, its_from_two_sources

start_time = time.time()

# import parameters
prm = __import__(sys.argv[1])

print('reading metrics...')
x,y,z,tri,nsiglev,siglev,nsiglay,siglay,\
nemo_lon,nemo_lat,dx,dy,e3u_0,e3v_0,\
gdept_0,gdepw_0,gdepu,gdepv,tmask,umask,vmask,gdept_1d,nemo_h = nesting.read_metrics(
                                                        prm.fvcom_grd_file,
                                                        prm.fvcom_dep_file,
                                                        prm.fvcom_sigma_file,
                                                        prm.nemo_coord_file,
                                                        prm.nemo_mask_file,
                                                        prm.nemo_bathy_file,
                                                        prm.nemo_cut_i,
                                                        prm.nemo_cut_j)

print('listing nemo files...')
if prm.search_dir_flag:
    prm.nemo_file_list = nesting.list_nemo_files(prm.input_dir, prm.nemo_file_pattern, 'T')

print('its from nemo...')
tn,sn = nesting.its_from_nemo(x,y,tri,prm.fvcom_utmzone,
             nemo_lon, nemo_lat, dx,dy, tmask, gdept_0, nemo_h,
             prm.nemo_file_list,
             prm.fvcom_time_start,
             t_name=prm.t_name,
             s_name=prm.s_name,
             file_exclude_poly=prm.file_exclude_poly)

print('its from jakes...')
jakesfile = '/media/krassovskim/MyPassport/Data/OSD/climatologies/nep35_ts_east_vi.mat'
zj,tj,sj = nesting.its_from_jakes_climatology(prm.fvcom_time_start,x,y,tri,prm.fvcom_utmzone,jakesfile)

print('merging...')
import numpy as np
xypoly = np.genfromtxt('/media/krassovskim/MyPassport/Max/Projects/opp/couple/climatology_poly.txt',delimiter=',',usecols=(0,1))
xyline = np.genfromtxt('/media/krassovskim/MyPassport/Max/Projects/opp/couple/climatology_ref_line.txt',delimiter=',',usecols=(0,1))
xr = xyline[:,0]; yr = xyline[:,1]
rwidth = 5000
t,s = nesting.its_from_two_sources(x,y, gdept_1d,tn,sn, zj,tj,sj, xypoly,xr,yr,rwidth)

print('writing file...')
fvt.make_observed_its(prm.output_file, prm.fvcom_time_start, -gdept_1d, t.T, s.T)

print("--- gen_its2 run time: %s seconds ---" % (time.time() - start_time))
# Control parameters for generation of FVCOM-NEMO nesting file.
# Supplied to gen_its.py

# FVCOM grid files
FVCOM_GRID_PATH = '/home/mif003/wvi_inlets4/'

# NEMO grid files
NEMO_GRID_PATH = '/home/mid002/WORK4/CIOPS-W-GRID/'


# Specify NEMO outputs where we find *T.nc files
# Option 1. Supply file list explicitly: nemo_file_list lists "T" NEMO output files.
#search_dir_flag = False
#nemo_file_list = ['/media/krassovskim/MyPassport/Data/opp/UBCFORCING/nowcast/21may18/FVCOM_T.nc',
#                  '/media/krassovskim/MyPassport/Data/opp/UBCFORCING/forecast/21may18/FVCOM_T.nc']

# Option 2. Search the specified input_dir for files according to nemo_file_pattern.
# input_dir can be a list of directories.
search_dir_flag = True
#input_dir = '/fs/hnas1-evs1/Ddfo/dfo_odis/jpp001/ECCC/CIOPS-W/nep36_SNDIST_04/' # full grid
input_dir = '/fs/hnas1-evs1/Ddfo/dfo_odis/jpp001/ECCC/CIOPS-W/pa_w_r1.5_bc12/'
nemo_file_pattern = '*_1d_grid_T_[0-9][0-9]*.nc' # full nemo grid

# Convert Conservative Temperature, Reference Salinity to in-situ temperature, Practical Salinity
do_conversion = False

fvcom_time_start = '2016-03-01 00:00:00'

t_name='thetao'
s_name='so'
#t_name = None
#s_name = None
#t_name = 'cons_temp'
#s_name = 'ref_salinity'

# Name for the forcing file to generate (_its.nc will be appended automatically):
output_file      = '/home/mak003/work4/projects/wvi/wvi_2016_03_01_BC12' #FVCOM_GRID_PATH+'wvi_2016_03_01'


#======== Case-independent =============

# Standard FVCOM grid files
fvcom_grd_file   = FVCOM_GRID_PATH+'wvi_inlets4_latlon_grd.dat'
fvcom_dep_file   = FVCOM_GRID_PATH+'wvi_inlets4_dep_coast-dep-reset_smp5_riv5m-v2.dat'
fvcom_sigma_file = FVCOM_GRID_PATH+'wvi_inlets4_sigma_v3.dat'
file_exclude_poly = None # COUPLING_PATH+'exclude_polygon.txt' # needed for full SSC grid
fvcom_utmzone = None #10

nemo_coord_file = None #NEMO_GRID_PATH+'coordinates_seagrid_SalishSea201702.nc'
nemo_mask_file  = NEMO_GRID_PATH+'mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'
nemo_bathy_file = NEMO_GRID_PATH+'Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'

# Indices limiting the output area. 
# For pyton array indexing: Zero-based, starting index inclusive, ending index exclusive.
# Use these variables in case NEMO output files in input_dir contain a trimmed area of 
# the full model grid defined in nemo_coord_file and nemo_mask_file. 
# Supply empty lists if the metrics files correspond to the output area.
nemo_cut_i = None # along x-axis
nemo_cut_j = None # along y-axis

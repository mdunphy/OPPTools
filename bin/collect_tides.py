from VH_stations import stations
from OPPTools.utils import getChsTideData

yr,mn = 2018, 01

d0 = '{}-{:02d}-01 00:00:00'.format(yr,mn)
d1 = '{}-{:02d}-03 00:00:00'.format(yr,mn)

for stnID in stations.keys():
    stnName = stations[stnID]['Name']
    print("\nWorking on station {}".format(stnName))
    dta = getChsTideData('obs', stnID, d0, d1, verbose=True)

    if dta.size > 0:
        fname = 'WaterLevel-{}-{}-{:02d}.txt'.format(stnID,yr,mn)
        dta.to_csv(fname, columns=['DATETIME','VALUE'], index=False)

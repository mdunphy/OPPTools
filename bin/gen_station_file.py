from pyproj import Proj
from VH_stations import stations, stations_chs
from OPPTools.utils import write_station_file


gridfile = 'vhfr_low_v2_utm10_grd.dat'
depthfile = 'vhfr_low_v2_utm10_dep.dat'
prj = Proj("+proj=utm +zone=10 +units=m")

fname = "vhfr_low_v2_utm10_station.dat"
write_station_file(stations, gridfile, depthfile, prj, fname)

fname = "vhfr_low_v2_utm10_station_chs.dat"
write_station_file(stations_chs, gridfile, depthfile, prj, fname)

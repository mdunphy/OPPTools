""" Example generation of FVCOM atmospheric forcing file.
"""

#import numpy as np
import OPPTools.fvcomToolbox as fvt
import OPPTools.atm as atm

username = 'user' # home inspiron
#username = 'krassovskim' # work

# FVCOM grid
opp_folder = '/media/'+username+'/MyPassport/Max/Projects/opp/'

# HRDPS data directory(-ies)
#hrdps_folder = '/media/'+username+'/MyPassport/Data/CMC/HRDPS'
hrdps_data = '/media/'+username+'/MyPassport/Data/opp/UBC-DATA/GRIB/'
hrdps_folder = [hrdps_data+'20170601',
                hrdps_data+'20170602',
                hrdps_data+'20170603']

grd = opp_folder+'vh_x2_grd.dat'
tri,nodes = fvt.readMesh_V3(grd)
x,y = nodes[:,0],nodes[:,1]
utmzone = 10 # UTM zone for x,y 

# time bounds for the forcing time series
tlim = ["2017-06-02 00:30:00","2017-06-03 00:30:00"]

# wind, heat flux or precip/evap file; options: 'wnd','hfx', 'hfx_const', or 'precip'
# 'hfx_const' doesn't use HRDPS output, but fills-in constant and uniform values
#ftype = 'hfx_const'
#constant_values=np.array([10, 101300, 70, 200, 250])
# 'air_temperature','air_pressure','relative_humidity','short_wave','long_wave'

for ftype1 in {'wnd','hfx','precip'}:
   # name for the forcing file to create
   fname = opp_folder+'vh_x2_test_'+ftype1+'.nc'

   atm.create_atm_hrdps(ftype1,x,y,tri,utmzone,tlim,fname,hrdps_folder)

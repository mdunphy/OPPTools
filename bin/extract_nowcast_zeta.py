"""
Script to extract sea level (zeta) from all vhfr_low_v2 nowcast runs 
on salish.eos.ubc.ca
"""
from OPPTools.utils import fvcom_postprocess as fpp

search_folder = '/opp/fvcom/nowcast/**/' # ** for recursive search in subfolders
casename = 'vhfr_low_v2'
output_file = casename+'_zeta_upto2019-01-03.nc'

files = fpp.nclist(search_folder,casename,sort_method='time')
fpp.ncextract(files,'zeta',output_file)
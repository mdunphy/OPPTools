"""
The program needs a parameter file (example is gen_its_param.py).
If the parameter file is gen_its_param.py, then the program is run like this:
python gen_its.py gen_its_param
"""
import sys
import time
import OPPTools.nesting as nesting
#import nesting_its as its


start_time = time.time()

# import parameters
prm = __import__(sys.argv[1])

x,y,z,tri,nsiglev,siglev,nsiglay,siglay,\
nemo_lon,nemo_lat,dx,dy,e3u_0,e3v_0,\
gdept_0,gdepw_0,gdepu,gdepv,tmask,umask,vmask,gdept_1d,nemo_h = nesting.read_metrics(
                                                        prm.fvcom_grd_file,
                                                        prm.fvcom_dep_file,
                                                        prm.fvcom_sigma_file,
                                                        prm.nemo_coord_file,
                                                        prm.nemo_mask_file,
                                                        prm.nemo_bathy_file,
                                                        prm.nemo_cut_i,
                                                        prm.nemo_cut_j)

if prm.search_dir_flag:
    prm.nemo_file_list = nesting.list_nemo_files(prm.input_dir, prm.nemo_file_pattern, 'T')
             
nesting.make_its(prm.output_file,x,y,tri,prm.fvcom_utmzone,
             nemo_lon, nemo_lat, dx,dy, tmask, gdept_0, nemo_h, gdept_1d,
             prm.nemo_file_list,
             prm.fvcom_time_start,
             t_name=prm.t_name,
             s_name=prm.s_name,
             file_exclude_poly=prm.file_exclude_poly,
             do_conversion=prm.do_conversion)

print("--- gen_nesting_file2 run time: %s seconds ---" % (time.time() - start_time))

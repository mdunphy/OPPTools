"""
Created on Tue Oct 16 16:46:45 2018

@author: krassovskim
"""

import numpy as np
from OPPTools.utils import fvcom_postprocess as fpp
from OPPTools.mesh import mesh

# tinterpolantc test
fname = '/media/krassovskim/MyPassport2/fvcom/hecate/run61/run61k/hecate5_0001.nc'
ncf = fpp.ncopen(fname)
x,y,z,tri,xc,yc,zc,siglay,_ = fpp.ncgrid(ncf)
nbe = ncf.variables['nbe'][:].T.copy() - 1
u = ncf.variables['u'][119,0,:] # 120th time step, sirface, all elements
v = ncf.variables['v'][119,0,:]

itri = 15950 # take one triangle with substantial change in velocities around it
xlim = [230000,237000]
ylim = [5487500,5494000]
figname = 'tinterpolantc_test_interior_python.png'
#    itri = 47554 # near open boundary (case with nans in we)
#    xlim = [129000,136000]
#    ylim = [5486000,5491200]
#    figname = 'tinterpolantc_test_boundary_python.png'

xi = x[tri[itri,:]]
yi = y[tri[itri,:]]
rx = np.max(xi) - np.min(xi) # range of coordinates for this triangle
ry = np.max(yi) - np.min(yi)
xxi = np.arange(np.min(xi)-rx/2, np.max(xi)+rx/2, 200) # expand range and discretize
yyi = np.arange(np.min(yi)-ry/2, np.max(yi)+ry/2, 200)
[xi,yi] = np.meshgrid(xxi,yyi);
xi = xi.ravel()
yi = yi.ravel()

k = mesh.tsearch(x,y,tri,xi,yi) # -1 for nodes outside the triangulation
[eid,we] = mesh.tinterpolantc(xc,yc,xi,yi,k,nbe)

ui = mesh.tri_interp_eval(eid,we,u)
vi = mesh.tri_interp_eval(eid,we,v)

# plot
from matplotlib import pyplot as plt
fig,ax = plt.subplots(figsize=(18, 12))
ax.set_aspect('equal')
plt.triplot(x,y,tri,Color='0.8',zorder=1)
plt.quiver(xc,yc, u, v,units='inches',scale_units='inches',scale=2,Color='r',zorder=2)
plt.quiver(xi,yi,ui,vi,units='inches',scale_units='inches',scale=2,Color='b',zorder=3)
ax.set_xlim(xlim)
ax.set_ylim(ylim)
plt.title('tinterpolantc test')
plt.show()
plt.savefig(figname)
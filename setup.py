from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

install_requires = [
    'netCDF4',
    'numpy',
    'utm',
    'zeep',
    'pyproj',
    'shapely'
]

setup(
    name='OPPTools',
    version='0.1.0',
    description='Python based tools for working with FVCOM for OPP',
    long_description=readme,
    author='Michael Dunphy, Maxim Krassovski, Pramod Thupaki',
    url='https://gitlab.com/mdunphy/OPPTools',
    install_requires=install_requires,
    packages=find_packages(exclude=('bin'))
)


""" Unit test
run as
python river_nodes_test.py -v
"""

import OPPTools.river as riv
import unittest

class TestMethods(unittest.TestCase):

    def test_river_nodes(self):
        self.assertEqual(riv.river_nodes('river_nodes_test_list.txt'), 
                         {'fraser': [34, 37, 22], 'lynn creek': [77, 88, 33], 'one': [10]})

if __name__ == '__main__':
    unittest.main()

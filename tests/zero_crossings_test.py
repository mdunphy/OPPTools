"""

"""
import numpy as np
import matplotlib.pyplot as plt
import OPPTools.general_utilities as gu

def zero_crossings_test():
    x = np.arange(0, 4, 0.1)
    y = np.sin(x*2)
    y[3:5] = 0
    y[-1] = 0
    x0,s = gu.zero_crossings(x,y)
    
    plt.plot(x,y,'.-')
    plt.plot(x0,np.zeros(len(x0)),'.r')
    plt.grid()
    plt.show()
    
    try:
        np.testing.assert_allclose(x0,np.array([0., 0.3, 0.4, 1.57073892, 3.14161991, 3.9]))
        np.testing.assert_allclose(s,np.array([ 0.,  0.,  0., -1.,  1.,  0.]))
    except AssertionError as err:
        print(err)
    else:
        print('zero_crossings_test passed')


if __name__ == '__main__':
    zero_crossings_test()
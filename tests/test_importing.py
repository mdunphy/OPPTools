"""
Simple tests to make sure that imports don't break
"""

def test_import_star():
    from OPPTools import *
    from OPPTools.fvcomToolbox import *
    from OPPTools.nesting import *
    from OPPTools.utils import *
    from OPPTools.atm import *
    from OPPTools.river import *
    from OPPTools.general_utilities import *
    assert True


def test_import_as():
    import OPPTools as ot
    import OPPTools.fvcomToolbox as fvc
    import OPPTools.nesting as nesting
    import OPPTools.utils as utils
    import OPPTools.atm as atm
    import OPPTools.river as riv
    import OPPTools.general_utilities as gutils
    assert True
